<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>FeatherPad::AboutDialog</name>
    <message>
        <location filename="../../about.ui" line="173"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
</context>
<context>
    <name>FeatherPad::FPwin</name>
    <message>
        <location filename="../../fpwin.cpp" line="481"/>
        <location filename="../../fpwin.cpp" line="1565"/>
        <location filename="../../fpwin.cpp" line="1627"/>
        <location filename="../../fpwin.cpp" line="2185"/>
        <location filename="../../fpwin.cpp" line="2247"/>
        <location filename="../../fpwin.cpp" line="3012"/>
        <location filename="../../fpwin.cpp" line="3032"/>
        <location filename="../../fpwin.cpp" line="3035"/>
        <location filename="../../fpwin.cpp" line="3986"/>
        <location filename="../../fpwin.cpp" line="4739"/>
        <location filename="../../fpwin.cpp" line="5033"/>
        <location filename="../../fpwin.cpp" line="5292"/>
        <source>Untitled</source>
        <translation>Namnlös</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="71"/>
        <source>Go to line:</source>
        <translation>Gå till rad:</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="107"/>
        <source>Select text from cursor to this line
(Ctrl+Shift+J)</source>
        <translation>Markera text från markör till denna rad
(Ctrl+Shift+J)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="111"/>
        <source>Select Text</source>
        <translation>Markera Text</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="114"/>
        <source>Ctrl+Shift+J</source>
        <translation>Ctrl+Shift+J</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="149"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="189"/>
        <location filename="../../fp.ui" line="980"/>
        <source>&amp;Edit</source>
        <translation>&amp;Redigera</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="219"/>
        <source>&amp;Options</source>
        <translation>&amp;Alternativ</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1588"/>
        <location filename="../../fpwin.cpp" line="2910"/>
        <location filename="../../fpwin.cpp" line="4549"/>
        <source>Encoding</source>
        <translation>Kodning</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="282"/>
        <source>&amp;Search</source>
        <translation>&amp;Sök</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="290"/>
        <location filename="../../fp.ui" line="746"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="390"/>
        <source>Find:</source>
        <translation>Hitta:</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="406"/>
        <source>To be replaced</source>
        <translation>Som skall ersättas</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="419"/>
        <source>Replace with:</source>
        <translation>Ersätt med:</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="435"/>
        <source>Replacing text</source>
        <translation>Ersätter text</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="494"/>
        <source>&amp;New</source>
        <translation>&amp;Ny</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="497"/>
        <source>New tab</source>
        <translation>Ny flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="500"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="505"/>
        <source>&amp;Open</source>
        <translation>&amp;Öppna</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="508"/>
        <source>Open a file</source>
        <translation>Öppna en fil</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="511"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="519"/>
        <location filename="../../fpwin.cpp" line="1363"/>
        <source>&amp;Save</source>
        <translation>&amp;Spara</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="522"/>
        <source>Save the current tab</source>
        <translation>Spara aktuell flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="525"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="533"/>
        <source>&amp;Undo</source>
        <translation>&amp;Ångra</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="536"/>
        <source>Undo</source>
        <translation>Ångra</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="539"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="547"/>
        <source>&amp;Redo</source>
        <translation>&amp;Gör om</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="550"/>
        <source>Redo</source>
        <translation>Gör om</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="553"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="561"/>
        <location filename="../../fp.ui" line="564"/>
        <source>Reload</source>
        <translation>Ladda om</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="567"/>
        <source>Ctrl+Shift+R</source>
        <translation>Ctrl+Shift+R</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="572"/>
        <source>&amp;Find</source>
        <translation>&amp;Hitta</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="578"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="586"/>
        <source>Show/hide replacement dock</source>
        <translation>Visa/dölj ersättning docka</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="589"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="594"/>
        <source>Save &amp;As</source>
        <translation>Spara &amp;som</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="597"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="602"/>
        <source>&amp;Print</source>
        <translation>&amp;Skriv ut</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="605"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="610"/>
        <source>Documen&amp;t Properties</source>
        <translation>Dokument &amp;egenskaper</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="613"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="618"/>
        <source>&amp;Close</source>
        <translation>&amp;Stäng</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="621"/>
        <source>Ctrl+Shift+Q</source>
        <translation>Ctrl+Shift+Q</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="626"/>
        <source>&amp;Quit</source>
        <translation>&amp;Avsluta</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="629"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="637"/>
        <source>&amp;Cut</source>
        <translation>&amp;Klipp ut</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="640"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="648"/>
        <source>C&amp;opy</source>
        <translation>&amp;Kopiera</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="651"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="656"/>
        <source>&amp;Paste</source>
        <translation>&amp;Klista in</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="659"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="667"/>
        <source>&amp;Delete</source>
        <translation>&amp;Ta bort</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="672"/>
        <source>&amp;Select All</source>
        <translation>&amp;Markera allt</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="675"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="680"/>
        <source>&amp;Font</source>
        <translation>&amp;Teckensnitt</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="688"/>
        <source>&amp;Line Numbers</source>
        <translation>&amp;Rad nummer</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="691"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="702"/>
        <source>&amp;Wrap Lines</source>
        <translation>&amp;Radbrytning</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="705"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="716"/>
        <source>&amp;Auto-Indentation</source>
        <translation>&amp;Automatisk indrag</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="719"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="730"/>
        <source>&amp;Syntax Highlighting</source>
        <translation>&amp;Syntaxframhävning</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="733"/>
        <source>Ctrl+Shift+H</source>
        <translation>Ctrl+Shift+H</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="738"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Inställningar</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="741"/>
        <source>Ctrl+Shift+P</source>
        <translation>Ctrl+Shift+P</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="749"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="754"/>
        <source>&amp;About</source>
        <translation>&amp;Om</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="781"/>
        <source>Enforce UTF-8</source>
        <translation>Tvinga UTF-8</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="964"/>
        <source>Save with &amp;Encoding</source>
        <translation>Spara med &amp;kodning</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="969"/>
        <source>&amp;Jump to</source>
        <translation>&amp;Gå till</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="972"/>
        <source>Show/hide jump bar</source>
        <translation>Visa/dölj hopprad</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="975"/>
        <source>Ctrl+J</source>
        <translation>Ctrl+J</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="983"/>
        <source>Edit text</source>
        <translation>Redigera text</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="986"/>
        <source>Ctrl+Shift+E</source>
        <translation>Ctrl+Shift+E</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1089"/>
        <source>&amp;Run</source>
        <translation>&amp;Kör</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1095"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1100"/>
        <source>&amp;Clear</source>
        <translation>Töm</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1103"/>
        <source>Clear the list of recently modified files</source>
        <translation>Töm listan av senaste modifierade filer</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1108"/>
        <source>Save/Restore Session</source>
        <translation>Spara/återskapa session</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1111"/>
        <source>Sa&amp;ve/Restore Session</source>
        <translation>&amp;Spara/återskapa session</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1114"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1119"/>
        <source>Side-Pane</source>
        <translation>Sidopanel</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1122"/>
        <source>Ctrl+Alt+P</source>
        <translation>Ctrl+Alt+P</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1127"/>
        <source>Paste Date and Time</source>
        <translation>Klista in datum och tid</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1130"/>
        <source>Ctrl+Shift+V</source>
        <translation>Ctrl+Shift+V</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1135"/>
        <location filename="../../fp.ui" line="1138"/>
        <source>To Upper Case</source>
        <translation>Till versaler</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1141"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Shift+U</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1146"/>
        <location filename="../../fp.ui" line="1149"/>
        <source>To Lower Case</source>
        <translation>Till gemener</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1152"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Shift+L</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1157"/>
        <location filename="../../fp.ui" line="1160"/>
        <source>To Start Case</source>
        <translation>Första bokstav versal</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1168"/>
        <location filename="../../fp.ui" line="1171"/>
        <source>Last Active Tab</source>
        <translation>Sista aktiva flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1174"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1179"/>
        <location filename="../../fp.ui" line="1182"/>
        <source>Sort Lines</source>
        <translation>Sortera rader</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1187"/>
        <location filename="../../fp.ui" line="1190"/>
        <source>Sort Lines Reversely</source>
        <translation>Sortera rader omvänt</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1195"/>
        <source>Check Spelling</source>
        <translation>Stavningskontroll</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1198"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1203"/>
        <location filename="../../fp.ui" line="1206"/>
        <source>Save All Files</source>
        <translation>Spara alla filer</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1211"/>
        <location filename="../../fp.ui" line="1214"/>
        <source>User Dictionary</source>
        <translation>Användarordlista</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1219"/>
        <source>Text Tabs to Spaces</source>
        <translation>Text tabulator till mellanslag</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="994"/>
        <source>&amp;Detach Tab</source>
        <translation>&amp;Lösgör flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="997"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1010"/>
        <source>Close Ne&amp;xt Tabs</source>
        <translation>Stäng &amp;nästa flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1015"/>
        <source>Close &amp;Previous Tabs</source>
        <translation>Stäng föregående flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1043"/>
        <source>Ne&amp;xt Tab</source>
        <translation>&amp;Nästa flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1054"/>
        <source>Previous Ta&amp;b</source>
        <translation>Föregående flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1092"/>
        <source>Execute</source>
        <translation>Exekvera</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1020"/>
        <source>Close &amp;All Tabs</source>
        <translation>Stänga &amp;alla flikar</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="153"/>
        <source>Recently &amp;Modified</source>
        <translation>Senaste &amp;filer</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="223"/>
        <source>&amp;Encoding</source>
        <translation>&amp;Kodning</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="227"/>
        <source>&amp;Unicode</source>
        <translation>&amp;Unicode</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="234"/>
        <source>&amp;Western European</source>
        <translation>&amp;Västeuropeisk</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="242"/>
        <source>&amp;East European</source>
        <translation>&amp;Östeuropeisk</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="250"/>
        <source>Ea&amp;st Asian</source>
        <translation>&amp;Östasiatisk</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="342"/>
        <location filename="../../fpwin.cpp" line="4089"/>
        <location filename="../../replace.cpp" line="61"/>
        <location filename="../../replace.cpp" line="113"/>
        <source>Replacement</source>
        <translation>Ersättning</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="575"/>
        <source>Focus/hide search bar</source>
        <translation>Focus/göm sök linje</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="583"/>
        <source>&amp;Replace</source>
        <translation>&amp;Ersätt</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="765"/>
        <source>Windows Arabic (&amp;CP1256)</source>
        <translation>Windows Arabisk (&amp;CP1256)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="773"/>
        <source>&amp;Other</source>
        <translation>&amp;Övriga</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="792"/>
        <source>&amp;UTF-8</source>
        <translation>&amp;UTF-8</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="803"/>
        <source>UTF-&amp;16</source>
        <translation>UTF-&amp;16</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="814"/>
        <source>&amp;ISO-8859-1</source>
        <translation>&amp;ISO-8859-1</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="825"/>
        <source>&amp;Windows-1252</source>
        <translation>&amp;Windows-1252</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="836"/>
        <source>&amp;Cyrillic (CP1251)</source>
        <translation>&amp;Kyrillisk (CP1251)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="847"/>
        <source>Cyrillic (&amp;KOI8-U)</source>
        <translation>Kyrillisk (&amp;KOI8-U)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="858"/>
        <source>Cyrillic (&amp;ISO-8859-5)</source>
        <translation>Kyrillisk (&amp;ISO-8859-5)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="869"/>
        <source>&amp;Chinese (BIG5)</source>
        <translation>&amp;Kinesisk (BIG5)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="880"/>
        <source>Chinese (&amp;GB18030)</source>
        <translation>Kinesisk (&amp;GB18030)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="891"/>
        <source>&amp;Japanese (ISO-2022-JP)</source>
        <translation>&amp;Japansk (ISO-2022-JP)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="902"/>
        <source>Japanese (&amp;ISO-2022-JP-2)</source>
        <translation>Japansk (&amp;ISO-2022-JP-2)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="913"/>
        <source>Japanese (ISO-&amp;2022-KR)</source>
        <translation>Japansk (ISO-&amp;2022-KR)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="924"/>
        <source>Ja&amp;panese (CP932)</source>
        <translation>Ja&amp;pansk (CP932)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="935"/>
        <source>Japa&amp;nese (EUC-JP)</source>
        <translation>Japa&amp;nsk (EUC-JP)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="943"/>
        <source>&amp;Korean (CP949)</source>
        <translation>&amp;Koreansk (CP949)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="951"/>
        <source>K&amp;orean (CP1361)</source>
        <translation>K&amp;oreansk (CP1361)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="959"/>
        <source>Korean (&amp;EUC-KR)</source>
        <translation>Koreansk (&amp;EUC-KR)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1005"/>
        <source>ISO-&amp;8859-15</source>
        <translation>ISO-&amp;8859-15</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1025"/>
        <source>Close &amp;Other Tabs</source>
        <translation>Stäng &amp;andra flikar</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1030"/>
        <source>&amp;Copy File Name</source>
        <translation>&amp;Kopiera filnamn</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1035"/>
        <source>Copy File &amp;Path</source>
        <translation>Kopiera filsökväg</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1046"/>
        <source>Alt+Right</source>
        <translation>Alt+Höger</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1057"/>
        <source>Alt+Left</source>
        <translation>Alt+Vänster</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1065"/>
        <source>&amp;First Tab</source>
        <translation>&amp;Första flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1068"/>
        <source>Alt+Down</source>
        <translation>Alt+Ner</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1076"/>
        <source>&amp;Last Tab</source>
        <translation>&amp;Sista flik</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1079"/>
        <source>Alt+Up</source>
        <translation>Alt+Upp</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1084"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="102"/>
        <source>Calculate number of words
(For huge texts, this may be CPU-intensive.)</source>
        <translation>Räkna ord (detta kan vara CPU-intensivt, om texten är lång)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="117"/>
        <source>Next</source>
        <translation>Nästa</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="118"/>
        <source>Previous</source>
        <translation>Föregående</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="119"/>
        <source>Replace all</source>
        <translation>Ersätt alla</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="643"/>
        <source>&amp;Recently Opened</source>
        <translation>&amp;Senaste öppnad</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2025"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1349"/>
        <source>Save changes?</source>
        <translation>Spara ändringar?</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1001"/>
        <source>Please attend to that window or just close its dialog!</source>
        <translation>Vänligen närvara till det fönster eller stäng dess dialog!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1353"/>
        <source>The document has been modified.</source>
        <translation>Dokumentet har blivit modifierad</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1364"/>
        <source>&amp;Discard changes</source>
        <translation>&amp;Förkasta ändringar</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1365"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1367"/>
        <source>&amp;No to all</source>
        <translation>&amp;Nej till alla</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1570"/>
        <location filename="../../fpwin.cpp" line="1628"/>
        <source>Unsaved</source>
        <translation>Osparad</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1589"/>
        <location filename="../../fpwin.cpp" line="2912"/>
        <location filename="../../fpwin.cpp" line="3635"/>
        <location filename="../../fpwin.cpp" line="3646"/>
        <location filename="../../fpwin.cpp" line="3653"/>
        <location filename="../../fpwin.cpp" line="4554"/>
        <location filename="../../fpwin.cpp" line="6146"/>
        <location filename="../../fpwin.cpp" line="6157"/>
        <location filename="../../fpwin.cpp" line="6164"/>
        <source>Lines</source>
        <translation>Wiersze</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1590"/>
        <location filename="../../fpwin.cpp" line="4555"/>
        <location filename="../../fpwin.cpp" line="4570"/>
        <source>Sel. Chars</source>
        <translation>Markerade tecken</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1591"/>
        <location filename="../../fpwin.cpp" line="4557"/>
        <location filename="../../fpwin.cpp" line="4571"/>
        <source>Words</source>
        <translation>Ord</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1918"/>
        <source>Another process is running in this tab!</source>
        <translation>En annan process körs i denna flik!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2016"/>
        <source>Script File</source>
        <translation>Script-fil</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1000"/>
        <source>Another FeatherPad window has a modal dialog!</source>
        <translation>Ett annat  FeatherPad fönster har en modal dialog!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="837"/>
        <location filename="../../fpwin.cpp" line="4596"/>
        <source>Position:</source>
        <translation>Placering:</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="885"/>
        <location filename="../../fpwin.cpp" line="4614"/>
        <location filename="../../fpwin.cpp" line="4634"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1351"/>
        <location filename="../../fpwin.cpp" line="2681"/>
        <location filename="../../fpwin.cpp" line="5956"/>
        <source>The file does not exist.</source>
        <translation>Filen existerar inte.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1720"/>
        <source>Open Link</source>
        <translation>Öppna flik</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1733"/>
        <source>Copy Link</source>
        <translation>Kopiera länk</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1919"/>
        <source>Only one process is allowed per tab.</source>
        <translation>Bara en process är tillåten per flik.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2012"/>
        <source>Script Output</source>
        <translation>Script-output</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2028"/>
        <source>Clear</source>
        <translation>Töm</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2638"/>
        <source>FeatherPad does not open files larger than 100 MiB.</source>
        <translation>FeatherPad öppnar inte filer större än 100 MiB.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2646"/>
        <source>Non-text file(s) not opened!</source>
        <translation>Fil(er) som inte är text kan inte öppnas!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2647"/>
        <source>See Preferences → Files → Do not permit opening of non-text files</source>
        <translation>Se Inställningar → Filer → Tillåt inte öppning av icke-text filer</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2656"/>
        <source>Some file(s) could not be opened!</source>
        <translation>Somliga fil(er) kunde inte öppnas!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2657"/>
        <source>You may not have the permission to read.</source>
        <translation>Du har inte rättighet att läsa.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2667"/>
        <source>Uneditable file(s)!</source>
        <translation>Icke-redigerbar fil(er)!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2668"/>
        <source>Non-text files or files with huge lines cannot be edited.</source>
        <translation>Icke-text fil(er) med ofantliga rader går inte redigeras.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2732"/>
        <source>Root Instance</source>
        <translation>Root instans</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2804"/>
        <location filename="../../fpwin.cpp" line="2993"/>
        <location filename="../../fpwin.cpp" line="3021"/>
        <source>All Files (*)</source>
        <translation>Alla filer (*)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2809"/>
        <source>All Files (*);;.%1 Files (*.%1)</source>
        <translation>Alla filer (*);;.%1 filer (*.%1)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2813"/>
        <source>Open file...</source>
        <translation>Öppna fil...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2999"/>
        <source>.%1 Files (*.%1);;All Files (*)</source>
        <translation>.%1 filer (*.%1);;Alla filer (*)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3050"/>
        <location filename="../../fpwin.cpp" line="3088"/>
        <source>Save as...</source>
        <translation>Spara som...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3124"/>
        <source>Keep encoding and save as...</source>
        <translation>Bevara kodning och spara som...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3459"/>
        <source>Saving as root.</source>
        <translation>Spara som root.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3460"/>
        <source>Waiting for authentication...</source>
        <translation>Väntar på autentisering...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3579"/>
        <source>&quot;pkexec&quot; is not found. Please install Polkit!</source>
        <translation>&quot;pkexec&quot; hittades inte. Vänligen installera Polkit!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3239"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3240"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3241"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3242"/>
        <source>Do you want to use &lt;b&gt;MS Windows&lt;/b&gt; end-of-lines?</source>
        <translation>Vil du använda &lt;b&gt;MS Windows&lt;/b&gt; end-of-lines?</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3243"/>
        <source>This may be good for readability under MS Windows.</source>
        <translation>Detta kan vara bra för läsbarheten i MS Windows.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3356"/>
        <location filename="../../fpwin.cpp" line="3474"/>
        <location filename="../../fpwin.cpp" line="3578"/>
        <location filename="../../fpwin.cpp" line="3590"/>
        <source>Cannot be saved!</source>
        <translation>Kan inte sparas!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="479"/>
        <location filename="../../fpwin.cpp" line="3984"/>
        <location filename="../../fpwin.cpp" line="5031"/>
        <location filename="../../fpwin.cpp" line="5290"/>
        <location filename="../../fpwin.cpp" line="6362"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3631"/>
        <location filename="../../fpwin.cpp" line="3637"/>
        <location filename="../../fpwin.cpp" line="3645"/>
        <location filename="../../fpwin.cpp" line="4552"/>
        <location filename="../../fpwin.cpp" line="6142"/>
        <location filename="../../fpwin.cpp" line="6148"/>
        <location filename="../../fpwin.cpp" line="6156"/>
        <source>Syntax</source>
        <translation>Syntax</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2637"/>
        <source>Huge file(s) not opened!</source>
        <translation>Stora fil(er) öppnas inte!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3734"/>
        <source>Text tabs are converted to spaces.</source>
        <translation>Text tabulatorer konverteres til mellanslag.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3836"/>
        <source>The selected text was too long.</source>
        <translation>Den markerade texten är för lång.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3837"/>
        <source>It is not fully processed.</source>
        <translation>Den är inte färdig behandlad.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3996"/>
        <location filename="../../fpwin.cpp" line="4229"/>
        <source>This file has been modified elsewhere or in another way!</source>
        <translation>Denna fil har blivit modifierad någon annan stans!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3997"/>
        <location filename="../../fpwin.cpp" line="4230"/>
        <source>Please be careful about reloading or saving this document!</source>
        <translation>Var försiktig med att återladda eller spara detta dokument!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="4705"/>
        <source>Printing in progress...</source>
        <translation>Utskrivt pågår...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="4756"/>
        <source>Print Document</source>
        <translation>Skriv ut dokument</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="4762"/>
        <source>Printing completed.</source>
        <translation>Utskrivt fördig.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5420"/>
        <location filename="../../fpwin.cpp" line="5544"/>
        <source>%1 Pages</source>
        <translation>%1 sidor</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5451"/>
        <location filename="../../fpwin.cpp" line="5580"/>
        <source>Copy Target Path</source>
        <translation>Kopiera Mål Sökväg</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5455"/>
        <location filename="../../fpwin.cpp" line="5584"/>
        <source>Open Target Here</source>
        <translation>Öppna mål här</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5474"/>
        <location filename="../../fpwin.cpp" line="5604"/>
        <source>Copy Final Target Path</source>
        <translation>Kopiera Slutgiltig Mål Sökväg</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5478"/>
        <location filename="../../fpwin.cpp" line="5608"/>
        <source>Open Final Target Here</source>
        <translation>Öppna slutgiltig Mål Sökväg</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5498"/>
        <location filename="../../fpwin.cpp" line="5629"/>
        <source>Open Containing Folder</source>
        <translation>Öppna innehållande mapp</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5752"/>
        <source>You need to add a Hunspell dictionary.</source>
        <translation>Du måste lägga till en Hunspell ordbok.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5753"/>
        <location filename="../../fpwin.cpp" line="5760"/>
        <location filename="../../fpwin.cpp" line="5770"/>
        <source>See Preferences → Text → Spell Checking!</source>
        <translation>Se: Inställningar → Text → Stavningskontroll</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5759"/>
        <source>The Hunspell dictionary does not exist.</source>
        <translation>Hunspell ordboken existerar inte.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5769"/>
        <source>The Hunspell dictionary is not accompanied by an affix file.</source>
        <translation>Det medföljer inte en affix-fil till Hunspell ordboken</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5792"/>
        <location filename="../../fpwin.cpp" line="5810"/>
        <location filename="../../fpwin.cpp" line="5825"/>
        <source>No misspelling in document.</source>
        <translation>Inga stavningsfel i dokumentet.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5794"/>
        <location filename="../../fpwin.cpp" line="5812"/>
        <location filename="../../fpwin.cpp" line="5827"/>
        <source>No misspelling from text cursor.</source>
        <translation>Inga stavningsfel från textmarkören.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5843"/>
        <source>Spell Checking</source>
        <translation>Stavningskontroll</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6180"/>
        <source>Some files cannot be saved!</source>
        <translation>Några filer kan inte sparas!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6223"/>
        <source>Translators</source>
        <translation>Översättare</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6219"/>
        <source>A lightweight, tabbed, plain-text editor</source>
        <translation>En lättviktig,flikbaserad, textredigeringsprogram </translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6220"/>
        <source>based on Qt</source>
        <translation>baserad på Qt</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6221"/>
        <source>Author</source>
        <translation>Författare</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6222"/>
        <source>aka.</source>
        <translation>även känd som</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6223"/>
        <location filename="../../fpwin.cpp" line="6224"/>
        <source>About FeatherPad</source>
        <translation>Om FeatherPad</translation>
    </message>
    <message>
        <location filename="../../replace.cpp" line="242"/>
        <source>No Replacement</source>
        <translation>Ingen ersättning</translation>
    </message>
    <message>
        <location filename="../../replace.cpp" line="244"/>
        <source>One Replacement</source>
        <translation>En ersättning</translation>
    </message>
    <message numerus="yes">
        <location filename="../../replace.cpp" line="246"/>
        <source>%Ln Replacements</source>
        <translation>
            <numerusform>%Ln ersättning</numerusform>
            <numerusform>%Ln ersättningar</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../replace.cpp" line="250"/>
        <source>The first 1000 replacements are highlighted.</source>
        <translation>De första 1000 ersättningar är framhävda.</translation>
    </message>
    <message>
        <location filename="../../syntax.cpp" line="312"/>
        <source>The size limit for syntax highlighting is exceeded.</source>
        <translation>Storlek begränsningen för syntax framhävning är överskriden.</translation>
    </message>
</context>
<context>
    <name>FeatherPad::FileDialog</name>
    <message>
        <location filename="../../filedialog.h" line="49"/>
        <source>Ctrl+H</source>
        <comment>Toggle showing hidden files</comment>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../filedialog.h" line="50"/>
        <source>Alt+.</source>
        <comment>Toggle showing hidden files</comment>
        <translation>Alt+.</translation>
    </message>
</context>
<context>
    <name>FeatherPad::FontDialog</name>
    <message>
        <location filename="../../fontDialog.ui" line="14"/>
        <source>Select Font</source>
        <translation>Välj Typsnitt</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="38"/>
        <source>Programming Fonts</source>
        <translation>Typsnitt för programmerare</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="45"/>
        <location filename="../../fontDialog.ui" line="55"/>
        <source>Font:</source>
        <translation>Typsnitt:</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="69"/>
        <source>Size:</source>
        <translation>Storlek:</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="89"/>
        <source>Weight:</source>
        <translation>Vikt</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="103"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="108"/>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="113"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="118"/>
        <source>Black</source>
        <translation>Svart</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="126"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="149"/>
        <source>Preview</source>
        <translation>Förhandsgranska</translation>
    </message>
</context>
<context>
    <name>FeatherPad::LineEdit</name>
    <message>
        <location filename="../../lineedit.cpp" line="34"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>Töm Text (Ctrl+K)</translation>
    </message>
</context>
<context>
    <name>FeatherPad::PrefDialog</name>
    <message>
        <location filename="../../prefDialog.ui" line="14"/>
        <source>Preferences</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="30"/>
        <source>Window</source>
        <translation>Fönster</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="69"/>
        <source>Window Settings</source>
        <translation>Fönster Inställningar</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="75"/>
        <source>Remember window &amp;size on closing</source>
        <translation>Kom ihåg fönster storlek vid stängning</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="84"/>
        <location filename="../../prefDialog.ui" line="97"/>
        <location filename="../../prefDialog.ui" line="107"/>
        <location filename="../../prefDialog.ui" line="120"/>
        <source>Window frame is excluded.</source>
        <translation>Fönster ram är excluderad.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="87"/>
        <source>Start with this size: </source>
        <translation>Starta med denna storlek: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="100"/>
        <location filename="../../prefDialog.ui" line="123"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="110"/>
        <source>×</source>
        <translation>×</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="148"/>
        <source>Enforcing a window position is not recommended
and may not work with some window managers.</source>
        <translation>Tvinga en fönster position rekommenderas ej och kanske inte fungerar med somliga fönster-hanterare.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="152"/>
        <source>Remember window &amp;position on closing</source>
        <translation>Kom ihåg fönster position vid stängning</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="159"/>
        <source>Most suitable with sessions
but without tab functionality.</source>
        <translation>Mest lämplig med sessioner men utan flik funktionalitet.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="188"/>
        <source>Uncheck for 1/5 of the width.</source>
        <translation>Avmarkera för 1/5 av bredd.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="191"/>
        <source>Remember splitter position</source>
        <translation>Kom ihåg uppdelarens position</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="200"/>
        <source>Do not show &amp;toolbar</source>
        <translation>Dölj &amp;verktygsfältet</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="207"/>
        <source>If the menubar is hidden,
a menu button appears on the toolbar.</source>
        <translation>Om menyrad är dold, så visas en menu knapp på verktygsfältet.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="211"/>
        <source>Do not show &amp;menubar</source>
        <translation>Dölj menyrad</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="218"/>
        <source>Menubar accelerators are activated by Alt
and may interfere with customized shortcuts.</source>
        <translation>Menu acceleratorer är aktiverad av Alt och kan kollidera med skräddarsydda genvägar.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="222"/>
        <source>Disable menubar accelerators</source>
        <translation>Inaktivera menufältets acceleratorer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="229"/>
        <source>Internal icons are used by default.</source>
        <translation>Interna ikoner används som standard.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="232"/>
        <source>Use system icons where possible</source>
        <translation>Använd systemikoner där det är möjligt</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="239"/>
        <source>Hide search &amp;bar by default</source>
        <translation>Dölj &amp;sökfält som standard</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="246"/>
        <source>By default, each search entry
has a separate search history.</source>
        <translation>Som standard, varje sökpost har en separat sökhistorik</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="250"/>
        <source>Use a shared search history</source>
        <translation>Använd en delad sökhistorik</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="257"/>
        <source>Always show st&amp;atus bar</source>
        <translation>Visa alltid &amp;statusfält</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="282"/>
        <source>Show cursor position on status bar</source>
        <translation>Visa markörposition i statusfältet</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="293"/>
        <location filename="../../prefDialog.ui" line="303"/>
        <source>Will take effect after closing this dialog.</source>
        <translation>Träder i kraft när denna dialog stängs.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="296"/>
        <source>Tab position: </source>
        <translation>Tabulator placering: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="307"/>
        <source>North</source>
        <translation>Nord</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="312"/>
        <source>South</source>
        <translation>Syd</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="317"/>
        <source>West</source>
        <translation>Väst</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="322"/>
        <source>East</source>
        <translation>Ost</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="348"/>
        <source>This means that, for LTR, Alt+Right goes to the first tab
after the last tab is activated, and the same for Alt+Left.

Tab navigation with mouse wheel is not affected.</source>
        <translation>Detta betyder att för LTR, så går Alt+Höger till den första fliken after den sista fliken är aktiverad, och det samma för Alt+Vänster.

Fliknavigation med mushjulet påverkas ej.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="354"/>
        <source>Tab navigation wraps &amp;around</source>
        <translation>Fliknavigation omslages runt</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="374"/>
        <source>Close window on closing its last tab</source>
        <translation>Stäng fönster när sista fliken stängs</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="381"/>
        <source>By default, if a FeatherPad window exists on the
current desktop, files will be opened in its tabs.

However, some desktop environments may not
report that they have multiple desktops.</source>
        <translation>Filer öppnas som standard i flikar, om det finns ett FeatherPad-fönster på det nuvarande skrivbordet.
        
Men, somliga skrivbordsmiljöer rapporterar
kanske inte att de har flera skrivbord.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="388"/>
        <source>Always open in separate windows</source>
        <translation>Öppna alltid i separata fönster</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="395"/>
        <source>If this is checked, the file dialog provided by the current
desktop environment will be used instead of the Qt file dialog.

Some desktop environments, like KDE and LXQt, provide files dialogs.</source>
        <translation>Om detta är valt, kommer fildialogen som tillhandhålls av det nuvarande skrivbordsmiljön att användas istället för Qt-fildialogen.

Några skrivbordsmiljöer, som KDE och LXQt, tillhandahåller egna fildialoger.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="401"/>
        <source>Native file dialog</source>
        <translation>Inbyggda fil-dialoger</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="477"/>
        <source>Uncheck for Monospace.</source>
        <translation>Avmarkera för Monospace.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="480"/>
        <source>Remember &amp;font</source>
        <translation>Kom ihåg &amp;typsnitt</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="501"/>
        <source>This covers parentheses, braces, brackets and quotes.</source>
        <translation>Detta täcker paranteser, krullparenteser, vinkelparenteser och citationstecken.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="504"/>
        <source>Auto-&amp;bracket</source>
        <translation>Automatisk &amp;vinkelparenteser</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="511"/>
        <source>By default, a triple period is replaced with an ellipsis
and a double hyphen with a long dash while typing,
under proper circumstances.</source>
        <translation>Som standard ersättes trippel punkt med en ellips och dubbel bindessträck med en lång radlinje medans man skriver, 
under rätta omständigheter</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="516"/>
        <source>Replace triple periods and double hyphens while typing</source>
        <translation>Ersätt trippla punkter och dubbla bindessträck medans man skriver</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="530"/>
        <source>Highlight case-sensitive and whole matches
of the selected text.</source>
        <translation>Framhäv små och stora bokstäver av markerad text</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="534"/>
        <source>Selection highlighting</source>
        <translation>Framhävning av markering</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="597"/>
        <source>Never highlight syntax for files &gt; </source>
        <translation>Framhäv aldrig syntax för filer &gt; </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="566"/>
        <source>This creates a menu button on the
status bar for changing the syntax.</source>
        <translation>Detta skapar en menyknapp i statusfältet for att skifta syntax.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="570"/>
        <source>Support syntax override</source>
        <translation>Stöd för sidosättning av syntax</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="635"/>
        <source>Show spaces, tabs and tab lines
when the syntax is highlighted.</source>
        <translation>Visa mellanrum, tabulator och tabulatorrader när syntax är framhävdt.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="639"/>
        <source>Show whitespaces</source>
        <translation>Visa blanksteg</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="655"/>
        <location filename="../../prefDialog.ui" line="666"/>
        <source>The vertical position lines will be drawn only if
the editor font has a fixed pitch (like Monospace).</source>
        <translation>Vertikala positions raden kommer endast bli ritad on editorns typsnitt har en fast teckenbredd (såsom Monospace).</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="659"/>
        <source>Show vertical lines starting from this position:</source>
        <translation>Visa vertikala rader från denna position:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="646"/>
        <source>Also show line and document ends</source>
        <translation>Visa också linje- och dokumentavslutningar</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="785"/>
        <source>Date and time format:</source>
        <translation>Datum- och klockslagformat:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="855"/>
        <source>Some text editors cannot open a document
whose last line is not empty.</source>
        <translation>Vissa textredigeringsprogram kan inte öppna dokument vars sista rad inte är tom.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="859"/>
        <source>Ensure an empty last line on saving</source>
        <translation>Säkerhetsställ en tom sista rad vid lagring</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="866"/>
        <source>Remove trailing spaces on saving</source>
        <translation>Avlägsna avslutande mellanslag vid lagring</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="884"/>
        <source>Inertial scrolling with mouse wheel</source>
        <translation>Tröghetsbaserad scrollning med mus hjulet</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="973"/>
        <source>Files</source>
        <translation>Filer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="979"/>
        <source>File Management</source>
        <translation>Fil Hantering</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1096"/>
        <source>Start with files of last window</source>
        <translation>Start med filer från sista fönster</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1138"/>
        <location filename="../../prefDialog.ui" line="1157"/>
        <source>This can be any starting command with
arguments, for example, &quot;xterm -hold&quot;
for running the process in XTerm.

If the command is left empty, the file
will be executed directly.

If the script is not run in a terminal
emulator, the output and error messages
will be shown by a popup dialog.</source>
        <translation>Detta kan vara vilken som helst
startkommando med argumenter, t.ex 
&quot;xterm -hold&quot; för att köra processen i XTerm.

Om kommodot utelämnas, so kommer 
filen bli exekverad direkt.

Om scriptet inte körs från en terminal emulator, 
så visas resultatet och fel meddelanden 
i en popup dialog.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1150"/>
        <source>Start with this command: </source>
        <translation>Starta med detta kommando: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1169"/>
        <source>Command + Arguments</source>
        <translation>Kommando + Argument</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1054"/>
        <source>Show recentl&amp;y modified files</source>
        <translation>&amp;Visa senaste modifierade filer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1084"/>
        <source>Show recentl&amp;y opened files</source>
        <translation>&amp;Visa senaste öppnade filer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1183"/>
        <source>Save changes to opened files every:</source>
        <translation>Spara ändringar till öppnade filer varje:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1461"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="163"/>
        <source>Start with side-pane mode</source>
        <translation>Starta i sidopanel-läge</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="361"/>
        <source>If this is checked, not only you will lose the informative
tooltip and context menu of a single tab but you could not
merge a single tabbed window into another one by tab drag-
and-drop either.</source>
        <translation>Om denna är ikryssad, inte bara du förlorar den informativa tooltip och context menu av en singel flik men du kan inte slå ihop a singel flik fönster in i en annan genom tabulator drag-and-drop.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="367"/>
        <source>&amp;Do not show a single tab</source>
        <translation>Visa inte en flik</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="432"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="471"/>
        <source>Text Editor</source>
        <translation>Text Editor</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="487"/>
        <source>&amp;Wrap lines by default</source>
        <translation>&amp;Bryt rader som standard</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="494"/>
        <source>Auto-&amp;indent by default</source>
        <translation>Automatisk &amp;indrag som standard</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="523"/>
        <source>Always show line &amp;numbers</source>
        <translation>Visa alltid &amp;rad-nummer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="541"/>
        <source>Highlight s&amp;yntax by default</source>
        <translation>Framhäv &amp;syntax som standard</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="701"/>
        <source>Dark c&amp;olor scheme</source>
        <translation>Mörk &amp;färg tema</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="736"/>
        <source>Background color value: </source>
        <translation>Backgrundens färgvärde: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="604"/>
        <source> MiB</source>
        <translation> MiB</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1103"/>
        <source>This only includes executable files written
in script languages like Shell and Python.

If it is checked and the opened file is
executable, the file will be executed by clicking
the Run button, that appears on the toolbar/filemenu
when needed, or by its shortcut Ctrl+E. Then, the
process could be killed by Ctrl+Alt+E.</source>
        <translation>Detta inkluderar endast exekverbara filer 
skrivna i script språk såsom Shell och Python.

Om den är ikryssad och den öppnade filer är exekverbar, kommer filen be exevkverad
genom klickning av Kör knappen, som visas på verktygsfältet/filmenuen
när det är nödvändigt, eller med den genväg  Ctrl+E. Då, 
kan processen avslutas genom Ctrl+Alt+E.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="698"/>
        <source>Needs window reopening to take effect.</source>
        <translation>Krävs att fönstet återöppnas för det träda i kraft.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="726"/>
        <location filename="../../prefDialog.ui" line="743"/>
        <source>The color value of the background.
255 means white while 0 means black.

For the light background, it can be
between 230 and 255; for the dark
background, between 0 and 50.

Needs window reopening to take effect.</source>
        <translation>Backgrundens färgvärde.
255 betyder vit och 0 betyder svart.

För en ljus bakgrund, kan värdet
vara mellan 230 och 255; för en mörk
bakgrund, mellan 0 och 50.

Krävs att fönstet återöppnas för att det träda i kraft.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1113"/>
        <source>Run executable scripts</source>
        <translation>Kör exekverbara script</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="987"/>
        <location filename="../../prefDialog.ui" line="1001"/>
        <source>The maximum number of recently modified or
opened files FeatherPad shows. The default
is 10.

Needs application restart to take effect.</source>
        <translation>Det maximala antalet av ändrade eller 
öppnade filer som FeatherPad viasar. Standard 
är 10.

Krävs en omstart av programmet för att detta ska träda i kraft.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="777"/>
        <location filename="../../prefDialog.ui" line="792"/>
        <source>Used for pasting the date and time.

Takes effect after closing this dialog.

Leave empty for:
MMM dd, yyyy, hh:mm:ss</source>
        <translation>Används för klista in datum och tid.

Träder i kraft efter dialogen stängs.

Låt vara tom för:
MMM dd, åååå, tt:mm:ss</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="814"/>
        <source>Text tab size:</source>
        <translation>Tabulatorstorlek för text:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="821"/>
        <source> spaces</source>
        <translation> mellanrum</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="873"/>
        <source>Thick text cursor</source>
        <translation>Fet textmarkör</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="880"/>
        <source>Should the mouse wheel scrolling be inertial
if the cursor is inside the text view?</source>
        <translation>Skall mushjulet rullning vara trög 
om markören är innanför textvisningen</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="891"/>
        <source>Spell Checking</source>
        <translation>Stavningskontroll</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="899"/>
        <location filename="../../prefDialog.ui" line="911"/>
        <source>A Hunspell dictionary has a name that ends with &quot;.dic&quot;
and should be alongside an affix file with the same
name but ending with &quot;.aff&quot;.</source>
        <translation>En Hunspell ordbok har ett name som slutar med &quot;.dic&quot;
och skall vara tillsammans med en affix file med samma 
name men slutar med &quot;.aff&quot;.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="904"/>
        <source>Hunspell dictionary path:</source>
        <translation>Sökväg till Hunspell-ordbok</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="923"/>
        <location filename="../../pref.cpp" line="1664"/>
        <source>Add dictionary...</source>
        <translation>Lägg till ordbok...</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="935"/>
        <source>If this is unchecked, spell checking
will be started from the document start.</source>
        <translation>Om detta inte är markerad, så startar stavningskontroll 
från dokumentets början</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="939"/>
        <source>Start spell checking from text cursor</source>
        <translation>Startar stavningskontroll från textmarkören</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="994"/>
        <source>Number of recent files: </source>
        <translation>Antal senaste filer: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1093"/>
        <source>Only if their number is not greater than 50.</source>
        <translation>Bara om antalet inte är större än 50.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1180"/>
        <location filename="../../prefDialog.ui" line="1190"/>
        <source>Only for files that exist and can be saved.</source>
        <translation>Bara för filer som existerar och kan sparas.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1193"/>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1224"/>
        <source>If this is checked, the Save button/menu-item could
be used also when the opened file is not modified.

This can be useful under certain circumstances,
e.g. for changing the time stamp or removing the
trailing spaces of a text file.</source>
        <translation>Om detta är ikryssad, kan spara knappen/menualternativet 
också användas när den öppnade filer inte är modifierad.

Detta kan vara användbart under vissa omständigheter, 
t.ex för att ändra tid stämpel eller avlägsna avslutande mellanrum i en textfil.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1232"/>
        <source>Allow saving unmodified files</source>
        <translation>Tillåt spara filer som inte är ändrade</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1239"/>
        <source>Show a warning instead.</source>
        <translation>Visa en varning istället.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1242"/>
        <source>Do not permit opening of non-text files</source>
        <translation>Tillåt inte öppning av icke text-filer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1249"/>
        <source>By default, when files are copied, they will
be opened if they are pasted into FeatherPad.</source>
        <translation>Som standard, när filer är kopieras, så 
öppnas dom som inklistrade i FeatherPad.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1253"/>
        <source>Paste paths instead of files</source>
        <translation>Klista in sökväg istället för filer</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1280"/>
        <source>Shortcuts</source>
        <translation>Genvägar</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1302"/>
        <source>Action</source>
        <translation>Handling</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1307"/>
        <source>Shortcut</source>
        <translation>Genväg</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1315"/>
        <source>Restore default shortcuts.</source>
        <translation>Återställ standard genvägar.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1318"/>
        <location filename="../../prefDialog.ui" line="1392"/>
        <location filename="../../prefDialog.ui" line="1440"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1329"/>
        <source>Syntax Colors</source>
        <translation>Syntax Färger</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1424"/>
        <source>Element</source>
        <translation>Element</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1429"/>
        <source>Text Color</source>
        <translation>Textfärg</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1346"/>
        <source>Whitespace color value:</source>
        <translation>Färgvärde för blanksteg:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1379"/>
        <location filename="../../prefDialog.ui" line="1389"/>
        <source>Has effect only if line numbers are shown.</source>
        <translation>Används endast on radnummer visas</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1382"/>
        <source>Current line highlight value:</source>
        <translation>Framhävningvärde för nuvarande rad:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1437"/>
        <source>Restore default syntax colors.</source>
        <translation>Återställ standardvärde för framhävningsfärg</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1472"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="104"/>
        <source>Press a modifier key to clear a shortcut
in the editing mode.</source>
        <translation>Tryck på en ändringstangent för att tömma en genväg 
i redigeringstillståndet</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="110"/>
        <source>Double click a color to change it.</source>
        <translation>Dubbelklicka på en färg för att ändra den</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="311"/>
        <location filename="../../pref.cpp" line="1360"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="311"/>
        <location filename="../../pref.cpp" line="1360"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="364"/>
        <location filename="../../pref.cpp" line="1479"/>
        <source>Warning: Ambiguous shortcut detected!</source>
        <translation>Varning: Tvetydig genväg registrerad!</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="397"/>
        <source>Functions, URLs,…</source>
        <translation>Funktioner, URLs,...</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="398"/>
        <source>Built-in Functions</source>
        <translation>Inbyggda funtioner</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="399"/>
        <source>Comments</source>
        <translation>Kommentarer</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="400"/>
        <source>Quotations</source>
        <translation>Citater</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="401"/>
        <source>Types</source>
        <translation>Typer</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="402"/>
        <source>Key Words</source>
        <translation>Nyckelord</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="403"/>
        <source>Numbers</source>
        <translation>Tal</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="404"/>
        <source>Regular Expressions, Code Blocks,…</source>
        <translation>Regulära uttryck (Regex), kodblocker,...</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="405"/>
        <source>Document Blocks, XML/HTML Elements,…</source>
        <translation>Dokumentblock, XML/HTML elementer, ...</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="406"/>
        <source>Markdown Headings, CSS Values,…</source>
        <translation>Mark-down rubriker, CSS-värder, ...</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="407"/>
        <source>Extra Elements</source>
        <translation>Extra elementer</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="568"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Programmet måste startas om för ändringar ska träda i kraft.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="588"/>
        <source>Window reopening is needed for changes to take effect.</source>
        <translation>Fönstret måste återöppnas för ändringar ska träda i kraft.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1386"/>
        <source>&amp;Recently Opened</source>
        <translation>&amp;Senaste öppnad</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1387"/>
        <source>Recently &amp;Modified</source>
        <translation>Senaste &amp;modifierad</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1462"/>
        <source>The typed shortcut was reserved.</source>
        <translation>Den inskrivna genvägen var reserverad.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1666"/>
        <source>Hunspell Dictionary Files (*.dic)</source>
        <translation>Hunspell-ordbokfiler (*.dic)</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="458"/>
        <location filename="../../pref.cpp" line="1752"/>
        <source>Select Syntax Color</source>
        <translation>Välj syntaxfärg</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SearchBar</name>
    <message>
        <location filename="../../searchbar.cpp" line="94"/>
        <location filename="../../searchbar.cpp" line="173"/>
        <source>Search...</source>
        <translation>Sök...</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="125"/>
        <source>Match Case</source>
        <translation>Matcha stora och små bokstäver</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="132"/>
        <source>Whole Word</source>
        <translation>Hela Ord</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="139"/>
        <source>Regular Expression</source>
        <translation>Regulära uttryck</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="171"/>
        <source>Search with regex...</source>
        <translation>Sök med regulära uttryck...</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="120"/>
        <source>Next</source>
        <translation>Nästa</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="121"/>
        <source>Previous</source>
        <translation>Föregående</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SessionDialog</name>
    <message>
        <location filename="../../sessionDialog.ui" line="14"/>
        <source>Session Manager</source>
        <translation>Sessions Hanterare</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="101"/>
        <location filename="../../sessionDialog.ui" line="270"/>
        <source>&amp;Remove</source>
        <translation>&amp;Ta bort</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="33"/>
        <source>&lt;b&gt;Save/Restore Session&lt;/b&gt;</source>
        <translation>&lt;b&gt;Spara/Återställ Session&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="56"/>
        <source>Filter...</source>
        <translation>Filter...</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="69"/>
        <source>Open the selected session(s).</source>
        <translation>Öppna valda session(er).</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="72"/>
        <location filename="../../sessionDialog.ui" line="275"/>
        <source>&amp;Open</source>
        <translation>&amp;Öppna</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="98"/>
        <location filename="../../sessionDialog.ui" line="108"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="115"/>
        <location filename="../../sessionDialog.ui" line="125"/>
        <source>Ctrl+Del</source>
        <translation>Ctrl+Del</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="159"/>
        <source>By default, all files that are opened in all
windows will be included in the saved session.</source>
        <translation>Som standard, alla filer som är öppna i 
alla fönster kommer inkluderas in den sparade sessionen.</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="163"/>
        <source>Save only from this &amp;window</source>
        <translation>Spara endast från detta &amp;fönster</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="180"/>
        <source>&lt;b&gt;Warning&lt;/b&gt;</source>
        <translation>&lt;b&gt;Varning&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="226"/>
        <location filename="../../session.cpp" line="328"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="233"/>
        <source>&amp;No</source>
        <translation>&amp;Nej</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="283"/>
        <source>Re&amp;name</source>
        <translation>&amp;Döp om</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="118"/>
        <source>Remove &amp;All</source>
        <translation>Ta bort &amp;alla</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="260"/>
        <source>&amp;Close</source>
        <translation>&amp;Stäng</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="135"/>
        <source>Save the current session under the given title.</source>
        <translation>Spara nuvarande session under den givna titeln.</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="138"/>
        <source>&amp;Save</source>
        <translation>&amp;Spara</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="149"/>
        <source>Type a name to save session</source>
        <translation>Namn för att spara session</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="179"/>
        <source>Nothing saved.&lt;br&gt;No file was opened.</source>
        <translation>Inget blev sparat.&lt;br&gt;Ingen fil var öppnad.</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="277"/>
        <source>No file exists or can be opened.</source>
        <translation>Ingen fil existerar eller kan öppnas.</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="279"/>
        <source>Not all files exist or can be opened.</source>
        <translation>Inte alla filer existerar eller kan öppnas .</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="314"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="333"/>
        <source>Do you really want to remove all saved sessions?</source>
        <translation>Vill du verkligen avlägsna alla sparade sessioner?</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="339"/>
        <source>Do you really want to remove the selected sessions?</source>
        <translation>Vill du verkligen avlägsna de valda/markerade sessioner?</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="341"/>
        <source>Do you really want to remove the selected session?</source>
        <translation>Vill du verkligen avlägsna den valda/markerade sessionen?</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="346"/>
        <source>A session with the same name exists.&lt;br&gt;Do you want to overwrite it?</source>
        <translation>En session med samma namn existerar?&lt;br&gt;Vill du skriva över den?</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SidePane</name>
    <message>
        <location filename="../../sidepane.cpp" line="210"/>
        <source>Filter...</source>
        <translation>Filter...</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SpellDialog</name>
    <message>
        <location filename="../../spellDialog.ui" line="25"/>
        <source>Unknown word:</source>
        <translation>Okänt ord</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="51"/>
        <source>Add To Dictionary</source>
        <translation>Lägg till i ordboken</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="62"/>
        <source>Replace with:</source>
        <translation>Ersätt med:</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="129"/>
        <source>Correct Once</source>
        <translation>Rätta en gång</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="142"/>
        <source>Correct All</source>
        <translation>Rätta alla</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="87"/>
        <source>Ignore Once</source>
        <translation>Ignorera en gång</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="100"/>
        <source>Ignore All</source>
        <translation>Ignorera alla</translation>
    </message>
</context>
<context>
    <name>FeatherPad::TextEdit</name>
    <message>
        <location filename="../../textedit.cpp" line="148"/>
        <source>Double click to center current line</source>
        <translation>Dubbelklicka för centrera nuvarande rad</translation>
    </message>
</context>
</TS>
