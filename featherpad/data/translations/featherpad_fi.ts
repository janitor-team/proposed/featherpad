<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>FeatherPad::AboutDialog</name>
    <message>
        <location filename="../../about.ui" line="173"/>
        <source>License</source>
        <translation>Lisenssi</translation>
    </message>
</context>
<context>
    <name>FeatherPad::FPwin</name>
    <message>
        <location filename="../../fpwin.cpp" line="481"/>
        <location filename="../../fpwin.cpp" line="1565"/>
        <location filename="../../fpwin.cpp" line="1627"/>
        <location filename="../../fpwin.cpp" line="2185"/>
        <location filename="../../fpwin.cpp" line="2247"/>
        <location filename="../../fpwin.cpp" line="3012"/>
        <location filename="../../fpwin.cpp" line="3032"/>
        <location filename="../../fpwin.cpp" line="3035"/>
        <location filename="../../fpwin.cpp" line="3986"/>
        <location filename="../../fpwin.cpp" line="4739"/>
        <location filename="../../fpwin.cpp" line="5033"/>
        <location filename="../../fpwin.cpp" line="5292"/>
        <source>Untitled</source>
        <translation>Nimetön</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="71"/>
        <source>Go to line:</source>
        <translation>Siirry riville:</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="107"/>
        <source>Select text from cursor to this line
(Ctrl+Shift+J)</source>
        <translation>Valitse teksti kursorin kohdalta tälle riville
(Ctrl+Shift+J)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="111"/>
        <source>Select Text</source>
        <translation>Valitse teksti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="114"/>
        <source>Ctrl+Shift+J</source>
        <translation>Ctrl+Shift+J</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="149"/>
        <source>&amp;File</source>
        <translation>&amp;Tiedosto</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="189"/>
        <location filename="../../fp.ui" line="980"/>
        <source>&amp;Edit</source>
        <translation>&amp;Muokkaa</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="219"/>
        <source>&amp;Options</source>
        <translation>&amp;Asetukset</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1588"/>
        <location filename="../../fpwin.cpp" line="2910"/>
        <location filename="../../fpwin.cpp" line="4549"/>
        <source>Encoding</source>
        <translation>Koodaus</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="282"/>
        <source>&amp;Search</source>
        <translation>&amp;Haku</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="290"/>
        <location filename="../../fp.ui" line="746"/>
        <source>&amp;Help</source>
        <translation>&amp;Ohje</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="390"/>
        <source>Find:</source>
        <translation>Etsi:</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="406"/>
        <source>To be replaced</source>
        <translation>Korvattava</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="419"/>
        <source>Replace with:</source>
        <translation>Korvaa:</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="435"/>
        <source>Replacing text</source>
        <translation>Korvaava</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="494"/>
        <source>&amp;New</source>
        <translation>&amp;Uusi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="497"/>
        <source>New tab</source>
        <translation>Uusi välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="500"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="505"/>
        <source>&amp;Open</source>
        <translation>&amp;Avaa</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="508"/>
        <source>Open a file</source>
        <translation>Avaa tiedosto</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="511"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="519"/>
        <location filename="../../fpwin.cpp" line="1363"/>
        <source>&amp;Save</source>
        <translation>&amp;Tallenna</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="522"/>
        <source>Save the current tab</source>
        <translation>Tallenna nykyinen välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="525"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="533"/>
        <source>&amp;Undo</source>
        <translation>&amp;Kumoa</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="536"/>
        <source>Undo</source>
        <translation>Kumoa</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="539"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="547"/>
        <source>&amp;Redo</source>
        <translation>&amp;Tee uudelleen</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="550"/>
        <source>Redo</source>
        <translation>Tee uudelleen</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="553"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="561"/>
        <location filename="../../fp.ui" line="564"/>
        <source>Reload</source>
        <translation>Lataa uudelleen</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="567"/>
        <source>Ctrl+Shift+R</source>
        <translation>Ctrl+Shift+R</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="572"/>
        <source>&amp;Find</source>
        <translation>&amp;Etsi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="578"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="586"/>
        <source>Show/hide replacement dock</source>
        <translation>Näytä/piilota korvaava telakka</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="589"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="594"/>
        <source>Save &amp;As</source>
        <translation>Tallenna &amp;nimellä</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="597"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="602"/>
        <source>&amp;Print</source>
        <translation>&amp;Tulosta</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="605"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="610"/>
        <source>Documen&amp;t Properties</source>
        <translation>&amp;Asiakirjan Ominaisuudet</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="613"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="618"/>
        <source>&amp;Close</source>
        <translation>&amp;Sulje</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="621"/>
        <source>Ctrl+Shift+Q</source>
        <translation>Ctrl+Shift+Q</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="626"/>
        <source>&amp;Quit</source>
        <translation>&amp;Lopeta</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="629"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="637"/>
        <source>&amp;Cut</source>
        <translation>&amp;Leikkaa</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="640"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="648"/>
        <source>C&amp;opy</source>
        <translation>&amp;Kopioi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="651"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="656"/>
        <source>&amp;Paste</source>
        <translation>&amp;Liitä</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="659"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="667"/>
        <source>&amp;Delete</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="672"/>
        <source>&amp;Select All</source>
        <translation>&amp;Valitse kaikki</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="675"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="680"/>
        <source>&amp;Font</source>
        <translation>&amp;Fontti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="688"/>
        <source>&amp;Line Numbers</source>
        <translation>&amp;Rivinumero</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="691"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="702"/>
        <source>&amp;Wrap Lines</source>
        <translation>&amp;Kääri Rivit</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="705"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="716"/>
        <source>&amp;Auto-Indentation</source>
        <translation>&amp;Automaattinen Sisennys</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="719"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="730"/>
        <source>&amp;Syntax Highlighting</source>
        <translation>&amp;Syntaksin Korostus</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="733"/>
        <source>Ctrl+Shift+H</source>
        <translation>Ctrl+Shift+H</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="738"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Asetukset</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="741"/>
        <source>Ctrl+Shift+P</source>
        <translation>Ctrl+Shift+P</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="749"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="754"/>
        <source>&amp;About</source>
        <translation>&amp;Tietoja</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="781"/>
        <source>Enforce UTF-8</source>
        <translation>Pakota UTF-8</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="964"/>
        <source>Save with &amp;Encoding</source>
        <translation>Tallenna &amp;Koodauksella</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="969"/>
        <source>&amp;Jump to</source>
        <translation>&amp;Hyppää</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="972"/>
        <source>Show/hide jump bar</source>
        <translation>Näytä/piilota hyppypalkki</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="975"/>
        <source>Ctrl+J</source>
        <translation>Ctrl+J</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="983"/>
        <source>Edit text</source>
        <translation>Muokkaa tekstiä</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="986"/>
        <source>Ctrl+Shift+E</source>
        <translation>Ctrl+Shift+E</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1089"/>
        <source>&amp;Run</source>
        <translation>&amp;Suorita</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1095"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1100"/>
        <source>&amp;Clear</source>
        <translation>&amp;Tyhjennä</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1103"/>
        <source>Clear the list of recently modified files</source>
        <translation>Tyhjennä viimeksi muokattujen tiedostojen lista</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1108"/>
        <source>Save/Restore Session</source>
        <translation>Tallenna/Palauta Istunto</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1111"/>
        <source>Sa&amp;ve/Restore Session</source>
        <translation>T&amp;allenna/Palauta Istunto</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1114"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1119"/>
        <source>Side-Pane</source>
        <translation>Sivupaneeli</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1122"/>
        <source>Ctrl+Alt+P</source>
        <translation>Ctrl+Alt+P</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1127"/>
        <source>Paste Date and Time</source>
        <translation>Liitä Päivämäärä ja Aika</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1130"/>
        <source>Ctrl+Shift+V</source>
        <translation>Ctrl+Shift+V</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1135"/>
        <location filename="../../fp.ui" line="1138"/>
        <source>To Upper Case</source>
        <translation>Isoiksi Kirjaimiksi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1141"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Shift+U</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1146"/>
        <location filename="../../fp.ui" line="1149"/>
        <source>To Lower Case</source>
        <translation>Pieniksi Kirjaimiksi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1152"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Shift+L</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1157"/>
        <location filename="../../fp.ui" line="1160"/>
        <source>To Start Case</source>
        <translation>Aloita Suurilla Kirjaimilla</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1168"/>
        <location filename="../../fp.ui" line="1171"/>
        <source>Last Active Tab</source>
        <translation>Viimeksi Aktiivinen Välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1174"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1179"/>
        <location filename="../../fp.ui" line="1182"/>
        <source>Sort Lines</source>
        <translation>Lajittele Rivit</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1187"/>
        <location filename="../../fp.ui" line="1190"/>
        <source>Sort Lines Reversely</source>
        <translation>Lajittele Rivit Takaperin</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1195"/>
        <source>Check Spelling</source>
        <translation>Tarkista Oikeinkirjoitus</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1198"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1203"/>
        <location filename="../../fp.ui" line="1206"/>
        <source>Save All Files</source>
        <translation>Tallenna kaikki tiedostot</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1211"/>
        <location filename="../../fp.ui" line="1214"/>
        <source>User Dictionary</source>
        <translation>Käyttäjän Sanakirja</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1219"/>
        <source>Text Tabs to Spaces</source>
        <translation>Sarkaimet Välilyönneiksi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="994"/>
        <source>&amp;Detach Tab</source>
        <translation>&amp;Irroita Välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="997"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1010"/>
        <source>Close Ne&amp;xt Tabs</source>
        <translation>Sulje Välilehdet &amp;Oikealta/Alta</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1015"/>
        <source>Close &amp;Previous Tabs</source>
        <translation>Sulje Välilehdet &amp;Vasemmalta/Yltä</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1043"/>
        <source>Ne&amp;xt Tab</source>
        <translation>&amp;Seuraava välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1054"/>
        <source>Previous Ta&amp;b</source>
        <translation>&amp;Edellinen välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1092"/>
        <source>Execute</source>
        <translation>Suorita</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1020"/>
        <source>Close &amp;All Tabs</source>
        <translation>Sulje &amp;Kaikki Välilehdet</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="153"/>
        <source>Recently &amp;Modified</source>
        <translation>Viimeksi &amp;Muokatut</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="223"/>
        <source>&amp;Encoding</source>
        <translation>&amp;Koodaus</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="227"/>
        <source>&amp;Unicode</source>
        <translation>&amp;Unicode</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="234"/>
        <source>&amp;Western European</source>
        <translation>&amp;Länsieurooppalainen</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="242"/>
        <source>&amp;East European</source>
        <translation>&amp;Itäeurooppalainen</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="250"/>
        <source>Ea&amp;st Asian</source>
        <translation>Itä&amp;aasialainen</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="342"/>
        <location filename="../../fpwin.cpp" line="4089"/>
        <location filename="../../replace.cpp" line="61"/>
        <location filename="../../replace.cpp" line="113"/>
        <source>Replacement</source>
        <translation>Korvaus</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="575"/>
        <source>Focus/hide search bar</source>
        <translation>Kohdista/piilota hakupalkki</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="583"/>
        <source>&amp;Replace</source>
        <translation>&amp;Korvaa</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="765"/>
        <source>Windows Arabic (&amp;CP1256)</source>
        <translation>Windows Arabialainen (&amp;CP1256)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="773"/>
        <source>&amp;Other</source>
        <translation>&amp;Muu</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="792"/>
        <source>&amp;UTF-8</source>
        <translation>&amp;UTF-8</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="803"/>
        <source>UTF-&amp;16</source>
        <translation>UTF-&amp;16</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="814"/>
        <source>&amp;ISO-8859-1</source>
        <translation>&amp;ISO-8859-1</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="825"/>
        <source>&amp;Windows-1252</source>
        <translation>&amp;Windows-1252</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="836"/>
        <source>&amp;Cyrillic (CP1251)</source>
        <translation>&amp;Kyrillinen (CP1251)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="847"/>
        <source>Cyrillic (&amp;KOI8-U)</source>
        <translation>K&amp;yrillinen (KOI8-U)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="858"/>
        <source>Cyrillic (&amp;ISO-8859-5)</source>
        <translation>Kyrillinen (&amp;ISO-8859-5)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="869"/>
        <source>&amp;Chinese (BIG5)</source>
        <translation>Kii&amp;nalainen (BIG5)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="880"/>
        <source>Chinese (&amp;GB18030)</source>
        <translation>Kiinalainen (&amp;GB18030)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="891"/>
        <source>&amp;Japanese (ISO-2022-JP)</source>
        <translation>&amp;Japanilainen (ISO-2022-JP)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="902"/>
        <source>Japanese (&amp;ISO-2022-JP-2)</source>
        <translation>Japanilainen (&amp;ISO-2022-JP-2)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="913"/>
        <source>Japanese (ISO-&amp;2022-KR)</source>
        <translation>Japanilainen (ISO-&amp;2022-KR)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="924"/>
        <source>Ja&amp;panese (CP932)</source>
        <translation>Ja&amp;panilainen (CP932)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="935"/>
        <source>Japa&amp;nese (EUC-JP)</source>
        <translation>Japa&amp;nilainen (EUC-JP)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="943"/>
        <source>&amp;Korean (CP949)</source>
        <translation>K&amp;orealainen (CP949)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="951"/>
        <source>K&amp;orean (CP1361)</source>
        <translation>Ko&amp;realainen (CP1361)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="959"/>
        <source>Korean (&amp;EUC-KR)</source>
        <translation>Korealainen (&amp;EUC-KR)</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1005"/>
        <source>ISO-&amp;8859-15</source>
        <translation>ISO-&amp;8859-15</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1025"/>
        <source>Close &amp;Other Tabs</source>
        <translation>Sulje &amp;Muut Välilehdet</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1030"/>
        <source>&amp;Copy File Name</source>
        <translation>&amp;Kopioi Tiedostonimi</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1035"/>
        <source>Copy File &amp;Path</source>
        <translation>Kopioi Tiedoston &amp;Polku</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1046"/>
        <source>Alt+Right</source>
        <translation>Alt+Oikealle</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1057"/>
        <source>Alt+Left</source>
        <translation>Alt+Vasemmalle</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1065"/>
        <source>&amp;First Tab</source>
        <translation>&amp;Ensimmäinen Välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1068"/>
        <source>Alt+Down</source>
        <translation>Alt+Alas</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1076"/>
        <source>&amp;Last Tab</source>
        <translation>&amp;Viimeinen Välilehti</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1079"/>
        <source>Alt+Up</source>
        <translation>Alt+Ylös</translation>
    </message>
    <message>
        <location filename="../../fp.ui" line="1084"/>
        <source>Menu</source>
        <translation>Valikko</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="102"/>
        <source>Calculate number of words
(For huge texts, this may be CPU-intensive.)</source>
        <translation>Laske sanojen lukumäärä
(Valtavat tekstimäärät voivat olla prosessorille raskaita.)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="117"/>
        <source>Next</source>
        <translation>Seuraava</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="118"/>
        <source>Previous</source>
        <translation>Edellinen</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="119"/>
        <source>Replace all</source>
        <translation>Korvaa kaikki</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="643"/>
        <source>&amp;Recently Opened</source>
        <translation>&amp;Viimeksi Avatut</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2025"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1349"/>
        <source>Save changes?</source>
        <translation>Tallennetaanko muutokset?</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1001"/>
        <source>Please attend to that window or just close its dialog!</source>
        <translation>Sulje, tai käytä jo olemassa olevaa valintaikkunaa!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1353"/>
        <source>The document has been modified.</source>
        <translation>Asiakirjaa on muokattu.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1364"/>
        <source>&amp;Discard changes</source>
        <translation>&amp;Hylkää muutokset</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1365"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Peruuta</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1367"/>
        <source>&amp;No to all</source>
        <translation>Vastaa &amp;ei kaikkiin</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1570"/>
        <location filename="../../fpwin.cpp" line="1628"/>
        <source>Unsaved</source>
        <translation>Tallentamaton</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1589"/>
        <location filename="../../fpwin.cpp" line="2912"/>
        <location filename="../../fpwin.cpp" line="3635"/>
        <location filename="../../fpwin.cpp" line="3646"/>
        <location filename="../../fpwin.cpp" line="3653"/>
        <location filename="../../fpwin.cpp" line="4554"/>
        <location filename="../../fpwin.cpp" line="6146"/>
        <location filename="../../fpwin.cpp" line="6157"/>
        <location filename="../../fpwin.cpp" line="6164"/>
        <source>Lines</source>
        <translation>Rivejä</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1590"/>
        <location filename="../../fpwin.cpp" line="4555"/>
        <location filename="../../fpwin.cpp" line="4570"/>
        <source>Sel. Chars</source>
        <translation>Merkkiä Valittuna</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1591"/>
        <location filename="../../fpwin.cpp" line="4557"/>
        <location filename="../../fpwin.cpp" line="4571"/>
        <source>Words</source>
        <translation>Sanoja</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1918"/>
        <source>Another process is running in this tab!</source>
        <translation>Toinen prosessi käyttää tätä välilehteä!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2016"/>
        <source>Script File</source>
        <translation>Skripti Tiedosto</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1000"/>
        <source>Another FeatherPad window has a modal dialog!</source>
        <translation>Toisella FeatherPad prosessilla on jo valintaikkuna auki!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="837"/>
        <location filename="../../fpwin.cpp" line="4596"/>
        <source>Position:</source>
        <translation>Sijainti:</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="885"/>
        <location filename="../../fpwin.cpp" line="4614"/>
        <location filename="../../fpwin.cpp" line="4634"/>
        <source>Normal</source>
        <translation>Normaali</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1351"/>
        <location filename="../../fpwin.cpp" line="2681"/>
        <location filename="../../fpwin.cpp" line="5956"/>
        <source>The file does not exist.</source>
        <translation>Tiedostoa ei löydy.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1720"/>
        <source>Open Link</source>
        <translation>Avaa Linkki</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1733"/>
        <source>Copy Link</source>
        <translation>Kopioi Linkki</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="1919"/>
        <source>Only one process is allowed per tab.</source>
        <translation>Vain yksi prosessi välilehteä kohden on sallittu.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2012"/>
        <source>Script Output</source>
        <translation>Skriptin Tuloste</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2028"/>
        <source>Clear</source>
        <translation>Tyhjennä</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2638"/>
        <source>FeatherPad does not open files larger than 100 MiB.</source>
        <translation>FeatherPad ei avaa suurempia tiedostoja kuin 100MiB.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2646"/>
        <source>Non-text file(s) not opened!</source>
        <translation>Tiedostoja, jotka eivät ole tekstitiedostoja ei avattu!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2647"/>
        <source>See Preferences → Files → Do not permit opening of non-text files</source>
        <translation>Asetukset → Tiedostot → Salli vain tekstitiedostojen avaaminen</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2656"/>
        <source>Some file(s) could not be opened!</source>
        <translation>Tiedostoa tai tiedostoja ei voitu avata!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2657"/>
        <source>You may not have the permission to read.</source>
        <translation>Lukuoikeus saattaa puuttua.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2667"/>
        <source>Uneditable file(s)!</source>
        <translation>Tiedostoa tai tiedostoja ei voida muokata!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2668"/>
        <source>Non-text files or files with huge lines cannot be edited.</source>
        <translation>Tiedostoja, jotka eivät ole tekstitiedostoja, tai joissa on valtavia rivejä ei voida muokata.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2732"/>
        <source>Root Instance</source>
        <translation>Root Instanssi</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2804"/>
        <location filename="../../fpwin.cpp" line="2993"/>
        <location filename="../../fpwin.cpp" line="3021"/>
        <source>All Files (*)</source>
        <translation>Kaikki Tiedostot (*)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2809"/>
        <source>All Files (*);;.%1 Files (*.%1)</source>
        <translation>Kaikki Tiedostot (*);;.%1 Tiedostot (*.%1)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2813"/>
        <source>Open file...</source>
        <translation>Avaa tiedosto...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2999"/>
        <source>.%1 Files (*.%1);;All Files (*)</source>
        <translation>.%1 Tiedostot (*.%1);;Kaikki Tiedostot (*)</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3050"/>
        <location filename="../../fpwin.cpp" line="3088"/>
        <source>Save as...</source>
        <translation>Tallenna nimellä...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3124"/>
        <source>Keep encoding and save as...</source>
        <translation>Säilytä koodaus ja tallenna nimellä...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3459"/>
        <source>Saving as root.</source>
        <translation>Tallennetaan root käyttäjänä.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3460"/>
        <source>Waiting for authentication...</source>
        <translation>Odotetaan oikeuksien tarkistamista...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3579"/>
        <source>&quot;pkexec&quot; is not found. Please install Polkit!</source>
        <translation>Polkit ohjelma täytyy olla asennettuna &quot;pkexec&quot; komentoa varten!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3239"/>
        <source>Yes</source>
        <translation>Kyllä</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3240"/>
        <source>No</source>
        <translation>Ei</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3241"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3242"/>
        <source>Do you want to use &lt;b&gt;MS Windows&lt;/b&gt; end-of-lines?</source>
        <translation>Haluatko käyttää &lt;b&gt;MS Windows&lt;/b&gt; tyylistä rivinvaihtoa?</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3243"/>
        <source>This may be good for readability under MS Windows.</source>
        <translation>Tämä voi auttaa luettavuuden kanssa MS Windowsia käytettäessä.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3356"/>
        <location filename="../../fpwin.cpp" line="3474"/>
        <location filename="../../fpwin.cpp" line="3578"/>
        <location filename="../../fpwin.cpp" line="3590"/>
        <source>Cannot be saved!</source>
        <translation>Tallentaminen ei onnistu!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="479"/>
        <location filename="../../fpwin.cpp" line="3984"/>
        <location filename="../../fpwin.cpp" line="5031"/>
        <location filename="../../fpwin.cpp" line="5290"/>
        <location filename="../../fpwin.cpp" line="6362"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3631"/>
        <location filename="../../fpwin.cpp" line="3637"/>
        <location filename="../../fpwin.cpp" line="3645"/>
        <location filename="../../fpwin.cpp" line="4552"/>
        <location filename="../../fpwin.cpp" line="6142"/>
        <location filename="../../fpwin.cpp" line="6148"/>
        <location filename="../../fpwin.cpp" line="6156"/>
        <source>Syntax</source>
        <translation>Syntaksi</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="2637"/>
        <source>Huge file(s) not opened!</source>
        <translation>Valtavaa tiedostoa tai tiedostoja ei avattu!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3734"/>
        <source>Text tabs are converted to spaces.</source>
        <translation>Sarkainmerkit muunnetaan välilyönneiksi.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3836"/>
        <source>The selected text was too long.</source>
        <translation>Valittu teksti oli liian pitkä.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3837"/>
        <source>It is not fully processed.</source>
        <translation>Sitä ei ole täysin käsitelty.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3996"/>
        <location filename="../../fpwin.cpp" line="4229"/>
        <source>This file has been modified elsewhere or in another way!</source>
        <translation>Tätä tiedostoa on muutettu toisaalla, tai muulla tavalla!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="3997"/>
        <location filename="../../fpwin.cpp" line="4230"/>
        <source>Please be careful about reloading or saving this document!</source>
        <translation>Ole hyvä ja ole varovainen tätä asiakirjaa uudelleen ladatessa tai tallentaessa!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="4705"/>
        <source>Printing in progress...</source>
        <translation>Tulostaminen käynnissä...</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="4756"/>
        <source>Print Document</source>
        <translation>Tulosta Asiakirja</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="4762"/>
        <source>Printing completed.</source>
        <translation>Tulostus valmis.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5420"/>
        <location filename="../../fpwin.cpp" line="5544"/>
        <source>%1 Pages</source>
        <translation>%1 Sivua</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5451"/>
        <location filename="../../fpwin.cpp" line="5580"/>
        <source>Copy Target Path</source>
        <translation>Kopioi Kohdepolku</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5455"/>
        <location filename="../../fpwin.cpp" line="5584"/>
        <source>Open Target Here</source>
        <translation>Avaa Kohde Tässä</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5474"/>
        <location filename="../../fpwin.cpp" line="5604"/>
        <source>Copy Final Target Path</source>
        <translation>Kopioi Lopullinen Kohdepolku</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5478"/>
        <location filename="../../fpwin.cpp" line="5608"/>
        <source>Open Final Target Here</source>
        <translation>Avaa Lopullinen Kohde Tässä</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5498"/>
        <location filename="../../fpwin.cpp" line="5629"/>
        <source>Open Containing Folder</source>
        <translation>Avaa Sisältävä Kansio</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5752"/>
        <source>You need to add a Hunspell dictionary.</source>
        <translation>Hunspell sanakirja täytyy lisätä.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5753"/>
        <location filename="../../fpwin.cpp" line="5760"/>
        <location filename="../../fpwin.cpp" line="5770"/>
        <source>See Preferences → Text → Spell Checking!</source>
        <translation>Asetukset → Teksti → Oikeinkirjoituksen Tarkistus!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5759"/>
        <source>The Hunspell dictionary does not exist.</source>
        <translation>Hunspell sanakirjaa ei ole olemassa.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5769"/>
        <source>The Hunspell dictionary is not accompanied by an affix file.</source>
        <translation>Hunspell sanakirjan affiksitiedosto puuttuu.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5792"/>
        <location filename="../../fpwin.cpp" line="5810"/>
        <location filename="../../fpwin.cpp" line="5825"/>
        <source>No misspelling in document.</source>
        <translation>Ei kirjoitusvirheitä asiakirjassa.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5794"/>
        <location filename="../../fpwin.cpp" line="5812"/>
        <location filename="../../fpwin.cpp" line="5827"/>
        <source>No misspelling from text cursor.</source>
        <translation>Ei kirjoitusvirheitä tekstikursorilta.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="5843"/>
        <source>Spell Checking</source>
        <translation>Oikeinkirjoituksen Tarkistus</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6180"/>
        <source>Some files cannot be saved!</source>
        <translation>Joitakin tiedostoja ei voida tallentaa!</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6223"/>
        <source>Translators</source>
        <translation>Kääntäjät</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6219"/>
        <source>A lightweight, tabbed, plain-text editor</source>
        <translation>Kevyt, välilehdillä varustettu tekstieditori</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6220"/>
        <source>based on Qt</source>
        <translation>perustuen Qt-kirjastoon</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6221"/>
        <source>Author</source>
        <translation>Tekijä</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6222"/>
        <source>aka.</source>
        <translation>aka.</translation>
    </message>
    <message>
        <location filename="../../fpwin.cpp" line="6223"/>
        <location filename="../../fpwin.cpp" line="6224"/>
        <source>About FeatherPad</source>
        <translation>FeatherPad Tietoja</translation>
    </message>
    <message>
        <location filename="../../replace.cpp" line="242"/>
        <source>No Replacement</source>
        <translation>Ei Korvattavaa</translation>
    </message>
    <message>
        <location filename="../../replace.cpp" line="244"/>
        <source>One Replacement</source>
        <translation>Yksi Korvaus</translation>
    </message>
    <message numerus="yes">
        <location filename="../../replace.cpp" line="246"/>
        <source>%Ln Replacements</source>
        <translation>
            <numerusform>%Ln Korvausta</numerusform>
            <numerusform>%Ln Korvausta</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../replace.cpp" line="250"/>
        <source>The first 1000 replacements are highlighted.</source>
        <translation>Ensimmäiset 1000 korvausta ovat korostettu.</translation>
    </message>
    <message>
        <location filename="../../syntax.cpp" line="312"/>
        <source>The size limit for syntax highlighting is exceeded.</source>
        <translation>Syntaksikorostuksen kokoraja on ylitetty.</translation>
    </message>
</context>
<context>
    <name>FeatherPad::FileDialog</name>
    <message>
        <location filename="../../filedialog.h" line="49"/>
        <source>Ctrl+H</source>
        <comment>Toggle showing hidden files</comment>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../filedialog.h" line="50"/>
        <source>Alt+.</source>
        <comment>Toggle showing hidden files</comment>
        <translation>Alt+.</translation>
    </message>
</context>
<context>
    <name>FeatherPad::FontDialog</name>
    <message>
        <location filename="../../fontDialog.ui" line="14"/>
        <source>Select Font</source>
        <translation>Valitse Fontti</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="38"/>
        <source>Programming Fonts</source>
        <translation>Ohjelmointi Fontit</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="45"/>
        <location filename="../../fontDialog.ui" line="55"/>
        <source>Font:</source>
        <translation>Fontti:</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="69"/>
        <source>Size:</source>
        <translation>Koko:</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="89"/>
        <source>Weight:</source>
        <translation>Paksuus:</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="103"/>
        <source>Normal</source>
        <translation>Normaali</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="108"/>
        <source>Medium</source>
        <translation>Keskiraskas</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="113"/>
        <source>Bold</source>
        <translation>Lihavoitu</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="118"/>
        <source>Black</source>
        <translation>Musta</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="126"/>
        <source>Italic</source>
        <translation>Kursiivi</translation>
    </message>
    <message>
        <location filename="../../fontDialog.ui" line="149"/>
        <source>Preview</source>
        <translation>Esikatselu</translation>
    </message>
</context>
<context>
    <name>FeatherPad::LineEdit</name>
    <message>
        <location filename="../../lineedit.cpp" line="34"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>Poista teksti (Ctrl+K)</translation>
    </message>
</context>
<context>
    <name>FeatherPad::PrefDialog</name>
    <message>
        <location filename="../../prefDialog.ui" line="14"/>
        <source>Preferences</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="30"/>
        <source>Window</source>
        <translation>Ikkuna</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="69"/>
        <source>Window Settings</source>
        <translation>Ikkunan Asetukset</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="75"/>
        <source>Remember window &amp;size on closing</source>
        <translation>Muista ikkunan koko suljettaessa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="84"/>
        <location filename="../../prefDialog.ui" line="97"/>
        <location filename="../../prefDialog.ui" line="107"/>
        <location filename="../../prefDialog.ui" line="120"/>
        <source>Window frame is excluded.</source>
        <translation>Ikkunan reunoja ei oteta huomioon.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="87"/>
        <source>Start with this size: </source>
        <translation>Aloita koolla: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="100"/>
        <location filename="../../prefDialog.ui" line="123"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="110"/>
        <source>×</source>
        <translation>×</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="148"/>
        <source>Enforcing a window position is not recommended
and may not work with some window managers.</source>
        <translation>Ikkunan kohdistuksen pakottamista ei suositella
eikä se välttämättä toimi joillakin ikkunointiohjelmilla.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="152"/>
        <source>Remember window &amp;position on closing</source>
        <translation>Muista ikkunan paikka suljettaessa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="159"/>
        <source>Most suitable with sessions
but without tab functionality.</source>
        <translation>Käytännöllisin istuntojen kanssa
mutta ilman välilehtiä.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="188"/>
        <source>Uncheck for 1/5 of the width.</source>
        <translation>Tyhjänä valitsee 1/5 leveydestä.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="191"/>
        <source>Remember splitter position</source>
        <translation>Muista puolittajan paikka</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="200"/>
        <source>Do not show &amp;toolbar</source>
        <translation>Älä näytä työkalupalkkia</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="207"/>
        <source>If the menubar is hidden,
a menu button appears on the toolbar.</source>
        <translation>Jos valikkopalkki poistetaan käytöstä,
ilmestyy valikkonappi työkalupalkkiin.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="211"/>
        <source>Do not show &amp;menubar</source>
        <translation>Älä näytä valikkopalkkia</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="218"/>
        <source>Menubar accelerators are activated by Alt
and may interfere with customized shortcuts.</source>
        <translation>Valikkopalkin pikakomennot aktivoidaan
Alt painikkeella, joka saattaa vaikuttaa
muokattujen pikanäppäinten toimintaan.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="222"/>
        <source>Disable menubar accelerators</source>
        <translation>Poista valikkopalkin pikakomennot käytöstä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="229"/>
        <source>Internal icons are used by default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="232"/>
        <source>Use system icons where possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="239"/>
        <source>Hide search &amp;bar by default</source>
        <translation>Piilota hakupalkki oletuksena</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="246"/>
        <source>By default, each search entry
has a separate search history.</source>
        <translation>Oletuksena jokaisella haulla on
erillinen hakuhistoria.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="250"/>
        <source>Use a shared search history</source>
        <translation>Käytä jaettua hakuhistoriaa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="257"/>
        <source>Always show st&amp;atus bar</source>
        <translation>Näytä tilapalkki aina</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="282"/>
        <source>Show cursor position on status bar</source>
        <translation>Näytä kursorin sijainti tilapalkissa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="293"/>
        <location filename="../../prefDialog.ui" line="303"/>
        <source>Will take effect after closing this dialog.</source>
        <translation>Muutokset tulevat voimaan tämän valintaikkunan sulkeutuessa.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="296"/>
        <source>Tab position: </source>
        <translation>Välilehden sijainti: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="307"/>
        <source>North</source>
        <translation>Pohjoinen</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="312"/>
        <source>South</source>
        <translation>Etelä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="317"/>
        <source>West</source>
        <translation>Länsi</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="322"/>
        <source>East</source>
        <translation>Itä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="348"/>
        <source>This means that, for LTR, Alt+Right goes to the first tab
after the last tab is activated, and the same for Alt+Left.

Tab navigation with mouse wheel is not affected.</source>
        <translation>Vasemmalta oikealle luettaessa, tämä tarkoittaa että
Alt+Oikea siirtyy ensimmäiseen välilehteen viimeiseltä,
ja Alt+Vasen ensimmäiseltä viimeiselle.

Tämä ei vaikuta välilehtien välillä siirtymiseen hiiren rullaa käyttämällä.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="354"/>
        <source>Tab navigation wraps &amp;around</source>
        <translation>Välilehtien välillä siirtyminen kiertää ympäri</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="374"/>
        <source>Close window on closing its last tab</source>
        <translation>Sulje ikkuna viimeisen välilehden sulkeutuessa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="381"/>
        <source>By default, if a FeatherPad window exists on the
current desktop, files will be opened in its tabs.

However, some desktop environments may not
report that they have multiple desktops.</source>
        <translation>Oletuksena tiedostot avataan uuteen välilehteen
työpöydällä olemassa olevaan FeatherPad ikkunaan.

Jotkin työpöytäympäristöt eivät välttämättä ilmoita
ohjelmistoille käyttävänsä useita eri työpöytiä.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="388"/>
        <source>Always open in separate windows</source>
        <translation>Avaa aina uuteen ikkunaan</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="395"/>
        <source>If this is checked, the file dialog provided by the current
desktop environment will be used instead of the Qt file dialog.

Some desktop environments, like KDE and LXQt, provide files dialogs.</source>
        <translation>Kun tämä on valittuna, työpöytäympäristön omia valintaikkunoita
käytetään Qt:n ikkunoiden sijasta.

Jotkin ympäristöt, kuten KDE ja LXQt omaavat tälläisiä valikoita.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="401"/>
        <source>Native file dialog</source>
        <translation>Natiivi tiedoston valintaikkuna</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="477"/>
        <source>Uncheck for Monospace.</source>
        <translation>Poista valinta valitaksesi Monospace.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="480"/>
        <source>Remember &amp;font</source>
        <translation>Muista fontti</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="501"/>
        <source>This covers parentheses, braces, brackets and quotes.</source>
        <translation>Tämä kattaa sulut, aaltosulut, hakasulut, sekä lainausmerkit.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="504"/>
        <source>Auto-&amp;bracket</source>
        <translation>Automaattiset hakasulut</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="511"/>
        <source>By default, a triple period is replaced with an ellipsis
and a double hyphen with a long dash while typing,
under proper circumstances.</source>
        <translation>Oletuksena kolme pistettä korvataan ellipsillä, ja
kaksi väliviivaa pitkällä viivalla tietyissä olosuhteissa.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="516"/>
        <source>Replace triple periods and double hyphens while typing</source>
        <translation>Korvaa kolme pistettä ja kaksi väliviivaa kirjoittaessa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="530"/>
        <source>Highlight case-sensitive and whole matches
of the selected text.</source>
        <translation>Korosta valittu teksti yhteenosuvuudesta
ja merkkikoosta riippuen.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="534"/>
        <source>Selection highlighting</source>
        <translation>Valitun tekstin korostus</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="597"/>
        <source>Never highlight syntax for files &gt; </source>
        <translation>Poista syntaksikorostus tiedostoille &gt; </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="566"/>
        <source>This creates a menu button on the
status bar for changing the syntax.</source>
        <translation>Tämä luo painikkeen tilariville
syntaksin muuttamista varten.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="570"/>
        <source>Support syntax override</source>
        <translation>Tue syntaksin ohittamista</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="635"/>
        <source>Show spaces, tabs and tab lines
when the syntax is highlighted.</source>
        <translation>Näytä välit, sarkaimet, ja sarkainrivit
kun syntaksi on korostettuna.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="639"/>
        <source>Show whitespaces</source>
        <translation>Näytä tyhjät merkit</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="655"/>
        <location filename="../../prefDialog.ui" line="666"/>
        <source>The vertical position lines will be drawn only if
the editor font has a fixed pitch (like Monospace).</source>
        <translation>Paikan ilmoittavat pystyviivat näytetään vain jos
käytössä on tasalevyinen kirjasin (kuten Monospace).</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="659"/>
        <source>Show vertical lines starting from this position:</source>
        <translation>Näytä pystyrivit alkaen kohdasta:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="646"/>
        <source>Also show line and document ends</source>
        <translation>Näytä myös rivien sekä asiakirjan loput</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="785"/>
        <source>Date and time format:</source>
        <translation>Päivämäärän ja ajan muoto:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="855"/>
        <source>Some text editors cannot open a document
whose last line is not empty.</source>
        <translation>Jotkin tekstieditorit evät pysty avaamaan
asiakirjoja jotka eivät lopu tyhjällä rivillä.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="859"/>
        <source>Ensure an empty last line on saving</source>
        <translation>Varmista tyhjä viimeinen rivi tallentaessa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="866"/>
        <source>Remove trailing spaces on saving</source>
        <translation>Poista tyhjät merkit rivien lopusta tallentaessa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="884"/>
        <source>Inertial scrolling with mouse wheel</source>
        <translation>Käytä inertiaa hiiren rullalla vierittäessä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="973"/>
        <source>Files</source>
        <translation>Tiedostot</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="979"/>
        <source>File Management</source>
        <translation>Tiedoston Hallinta</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1096"/>
        <source>Start with files of last window</source>
        <translation>Aloita viimeisen ikkunan tiedostoilla</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1138"/>
        <location filename="../../prefDialog.ui" line="1157"/>
        <source>This can be any starting command with
arguments, for example, &quot;xterm -hold&quot;
for running the process in XTerm.

If the command is left empty, the file
will be executed directly.

If the script is not run in a terminal
emulator, the output and error messages
will be shown by a popup dialog.</source>
        <translation>Tämä voi olla mikä tahansa komento
argumentteineen, esim. &quot;xterm -hold&quot;
joka suorittaa prosessin XTerm:issä.

Jos komento jätetään tyhjäksi, tiedosto
suoritetaan sellaisenaan.

Jos skriptiä ei suoriteta terminaali-
emulaattorissa, tulosteet ja virheet
näytetään ponnahdus ikkunassa.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1150"/>
        <source>Start with this command: </source>
        <translation>Aloita komennolla: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1169"/>
        <source>Command + Arguments</source>
        <translation>Komento + Argumentit</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1054"/>
        <source>Show recentl&amp;y modified files</source>
        <translation>Näytä viimeksi muokatut tiedostot</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1084"/>
        <source>Show recentl&amp;y opened files</source>
        <translation>Näytä viimeksi avatut tiedostot</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1183"/>
        <source>Save changes to opened files every:</source>
        <translation>Tallenna muutokset avattuihin tiedostoihin joka:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1461"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="163"/>
        <source>Start with side-pane mode</source>
        <translation>Aloita sivuruutu tilassa</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="361"/>
        <source>If this is checked, not only you will lose the informative
tooltip and context menu of a single tab but you could not
merge a single tabbed window into another one by tab drag-
and-drop either.</source>
        <translation>Kun tämä on valittuna, välilehden työkaluvihje,
valintaikkuna, sekä raahaaminen toiseen ikkunaan
poistetaan käytöstä.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="367"/>
        <source>&amp;Do not show a single tab</source>
        <translation>Älä näytä vain yhtä välilehteä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="432"/>
        <source>Text</source>
        <translation>Teksti</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="471"/>
        <source>Text Editor</source>
        <translation>Teksti Editori</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="487"/>
        <source>&amp;Wrap lines by default</source>
        <translation>Kiedo rivit oletuksena</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="494"/>
        <source>Auto-&amp;indent by default</source>
        <translation>Sisennä automaattisesti oletuksena</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="523"/>
        <source>Always show line &amp;numbers</source>
        <translation>Näytä aina rivien numerot</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="541"/>
        <source>Highlight s&amp;yntax by default</source>
        <translation>Korosta syntaksi oletuksena</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="701"/>
        <source>Dark c&amp;olor scheme</source>
        <translation>Tumma värijärjestelmä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="736"/>
        <source>Background color value: </source>
        <translation>Taustan väriarvo: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="604"/>
        <source> MiB</source>
        <translation> MiB</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1103"/>
        <source>This only includes executable files written
in script languages like Shell and Python.

If it is checked and the opened file is
executable, the file will be executed by clicking
the Run button, that appears on the toolbar/filemenu
when needed, or by its shortcut Ctrl+E. Then, the
process could be killed by Ctrl+Alt+E.</source>
        <translation>Tämä vaikuttaa ainoastaan suoritettaviin
tiedostoihin jotka on kirjoitettu käyttäen
skriptauskieliä kuten Shell ja Python.

Jos avattava tiedosto on merkitty suoritettavaksi,
tiedosto ajetaan kun työkalupalkkiin ilmestyvää
Suorita nappia painetaan, tai kun komento valitaan
Tiedosto valikosta. Lisäksi voidaan käyttää
näppäinyhdistelmää Ctrl+E. Prosessin suoritus voidaan
päättää painamalla Ctrl+Alt+E.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="698"/>
        <source>Needs window reopening to take effect.</source>
        <translation>Ikkunan uudelleen avaaminen vaaditaan muutosten voimaan tulemiseksi.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="726"/>
        <location filename="../../prefDialog.ui" line="743"/>
        <source>The color value of the background.
255 means white while 0 means black.

For the light background, it can be
between 230 and 255; for the dark
background, between 0 and 50.

Needs window reopening to take effect.</source>
        <translation>Taustan väriarvo. 255 tarkoittaa
valkoista, ja 0 tarkoittaa mustaa.

Vaaleaa taustaa varten voidaan
käyttää arvoja 230 - 255, ja
tummaa varten 0 - 50.

Vaatii ikkunan uudelleen avaamisen
jotta muutokset tulevat voimaan.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1113"/>
        <source>Run executable scripts</source>
        <translation>Aja suoritettavat skriptit</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="987"/>
        <location filename="../../prefDialog.ui" line="1001"/>
        <source>The maximum number of recently modified or
opened files FeatherPad shows. The default
is 10.

Needs application restart to take effect.</source>
        <translation>Viimeksi muokattujen tai avattujen tiedostojen
enimmäismäärä jonka FeatherPad näyttää.
Oletuksena tämä on 10.

Vaatii ohjelman uudelleen käynnistämisen
jotta muutokset tulevat voimaan.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="777"/>
        <location filename="../../prefDialog.ui" line="792"/>
        <source>Used for pasting the date and time.

Takes effect after closing this dialog.

Leave empty for:
MMM dd, yyyy, hh:mm:ss</source>
        <translation>Käytetään päivämäärän ja ajan lisäämiseen.

Tulee voimaan kun tämä valintaikkuna suljetaan.

Tyhjänä muoto on:
MMM dd, yyyy, hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="814"/>
        <source>Text tab size:</source>
        <translation>Sarkaimen koko:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="821"/>
        <source> spaces</source>
        <translation> väliä</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="873"/>
        <source>Thick text cursor</source>
        <translation>Leveä tekstikursori</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="880"/>
        <source>Should the mouse wheel scrolling be inertial
if the cursor is inside the text view?</source>
        <translation>Käytetäänkö hiiren rullalla vierittäessä inertiaa
kun kursori on tekstialueen sisällä?</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="891"/>
        <source>Spell Checking</source>
        <translation>Oikeinkirjoituksen tarkistus</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="899"/>
        <location filename="../../prefDialog.ui" line="911"/>
        <source>A Hunspell dictionary has a name that ends with &quot;.dic&quot;
and should be alongside an affix file with the same
name but ending with &quot;.aff&quot;.</source>
        <translation>Hunspell sanakirjalla on tiedostopäätteenä &quot;.dic&quot;,
ja sen mukaan kuuluu samanniminen affiksitiedosto 
jonka pääte on &quot;.aff&quot;.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="904"/>
        <source>Hunspell dictionary path:</source>
        <translation>Hunspell sanakirjan hakemistopolku:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="923"/>
        <location filename="../../pref.cpp" line="1664"/>
        <source>Add dictionary...</source>
        <translation>Lisää sanakirja...</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="935"/>
        <source>If this is unchecked, spell checking
will be started from the document start.</source>
        <translation>Jos tämä ei ole valittuna, oikeinkirjoituksen
tarkistaminen aloitetaan asiakirjan alusta.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="939"/>
        <source>Start spell checking from text cursor</source>
        <translation>Aloita tarkistaminen tekstikursorin kohdalta</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="994"/>
        <source>Number of recent files: </source>
        <translation>Viimeksikäytettyjen tiedostojen määrä: </translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1093"/>
        <source>Only if their number is not greater than 50.</source>
        <translation>Vain kun niiden lukumäärä ei ole yli 50.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1180"/>
        <location filename="../../prefDialog.ui" line="1190"/>
        <source>Only for files that exist and can be saved.</source>
        <translation>Vain olemassaoleville tiedostoille jotka voidaan tallentaa.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1193"/>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1224"/>
        <source>If this is checked, the Save button/menu-item could
be used also when the opened file is not modified.

This can be useful under certain circumstances,
e.g. for changing the time stamp or removing the
trailing spaces of a text file.</source>
        <translation>Kun tämä on valittuna, tallennus toimintoa
voidaan käyttää myös vaikka avattua tiedostoa
ei ole muokattu.

Tämä voi olla käytännöllistä esimerkiksi kun halutaan
muuttaa tiedoston aikaleimaa, tai kun poistetaan
tyhjiä merkkejä rivien lopusta.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1232"/>
        <source>Allow saving unmodified files</source>
        <translation>Salli muuttumattomien tiedostojen tallennus</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1239"/>
        <source>Show a warning instead.</source>
        <translation>Näytä varoitus.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1242"/>
        <source>Do not permit opening of non-text files</source>
        <translation>Salli ainoastaan tekstitiedostojen avaaminen</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1249"/>
        <source>By default, when files are copied, they will
be opened if they are pasted into FeatherPad.</source>
        <translation>Oletuksena FeatherPad avaa tiedostot jos
ne kopioidaan ja liitetään siihen.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1253"/>
        <source>Paste paths instead of files</source>
        <translation>Liitä polku, ei tiedostoja</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1280"/>
        <source>Shortcuts</source>
        <translation>Pikanäppäimet</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1302"/>
        <source>Action</source>
        <translation>Toiminto</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1307"/>
        <source>Shortcut</source>
        <translation>Pikanäppäin</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1315"/>
        <source>Restore default shortcuts.</source>
        <translation>Palauta oletus pikanäppäimet.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1318"/>
        <location filename="../../prefDialog.ui" line="1392"/>
        <location filename="../../prefDialog.ui" line="1440"/>
        <source>Default</source>
        <translation>Oletukset</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1329"/>
        <source>Syntax Colors</source>
        <translation>Syntaksivärit</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1424"/>
        <source>Element</source>
        <translation>Elementti</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1429"/>
        <source>Text Color</source>
        <translation>Tekstin Väri</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1346"/>
        <source>Whitespace color value:</source>
        <translation>Tyhjän merkin väriarvo:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1379"/>
        <location filename="../../prefDialog.ui" line="1389"/>
        <source>Has effect only if line numbers are shown.</source>
        <translation>Tällä on vaikutusta vain kun rivien numerot näytetään.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1382"/>
        <source>Current line highlight value:</source>
        <translation>Nykyisen rivin korostusarvo:</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1437"/>
        <source>Restore default syntax colors.</source>
        <translation>Palauta oletus syntaksivärit.</translation>
    </message>
    <message>
        <location filename="../../prefDialog.ui" line="1472"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="104"/>
        <source>Press a modifier key to clear a shortcut
in the editing mode.</source>
        <translation>Paina yhdistelmänäppäintä tyhjentääksesi
pikanäppäimen arvo editointitilassa.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="110"/>
        <source>Double click a color to change it.</source>
        <translation>Kaksoisklikkaa väriä muokataksesi sitä.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="311"/>
        <location filename="../../pref.cpp" line="1360"/>
        <source>files</source>
        <translation>tiedostoa</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="311"/>
        <location filename="../../pref.cpp" line="1360"/>
        <source>file</source>
        <translation>tiedosto</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="364"/>
        <location filename="../../pref.cpp" line="1479"/>
        <source>Warning: Ambiguous shortcut detected!</source>
        <translation>Varoitus: Samaa yhdistelmää käytetty useammin kuin kerran!</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="397"/>
        <source>Functions, URLs,…</source>
        <translation>Funktiot, URL:t,…</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="398"/>
        <source>Built-in Functions</source>
        <translation>Sisäänrakennetut Funktiot</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="399"/>
        <source>Comments</source>
        <translation>Kommentit</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="400"/>
        <source>Quotations</source>
        <translation>Lainaukset</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="401"/>
        <source>Types</source>
        <translation>Tyypit</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="402"/>
        <source>Key Words</source>
        <translation>Avainsanat</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="403"/>
        <source>Numbers</source>
        <translation>Numerot</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="404"/>
        <source>Regular Expressions, Code Blocks,…</source>
        <translation>Säännölliset Lausekkeet, Koodilohkot,…</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="405"/>
        <source>Document Blocks, XML/HTML Elements,…</source>
        <translation>Asiakirjalohkot, XML/HTML Elementit,…</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="406"/>
        <source>Markdown Headings, CSS Values,…</source>
        <translation>Markdown Otsikot, CSS Arvot,…</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="407"/>
        <source>Extra Elements</source>
        <translation>Extra Elementit</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="568"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Sovellus tulee käynnistää uudelleen, jotta muutokset tulevat voimaan.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="588"/>
        <source>Window reopening is needed for changes to take effect.</source>
        <translation>Vaatii ikkunan uudelleen avaamisen muutoksien voimaan tulemiseksi.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1386"/>
        <source>&amp;Recently Opened</source>
        <translation>&amp;Viimeksi avatut</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1387"/>
        <source>Recently &amp;Modified</source>
        <translation>Viimeksi &amp;muokatut</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1462"/>
        <source>The typed shortcut was reserved.</source>
        <translation>Syötetty yhdistelmä oli varattu.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="1666"/>
        <source>Hunspell Dictionary Files (*.dic)</source>
        <translation>Hunspell Sanakirja Tiedostot (*.dic)</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="458"/>
        <location filename="../../pref.cpp" line="1752"/>
        <source>Select Syntax Color</source>
        <translation>Valitse synktaksiväri</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SearchBar</name>
    <message>
        <location filename="../../searchbar.cpp" line="94"/>
        <location filename="../../searchbar.cpp" line="173"/>
        <source>Search...</source>
        <translation>Etsi...</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="125"/>
        <source>Match Case</source>
        <translation>Täsmää Kirjasinkoko</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="132"/>
        <source>Whole Word</source>
        <translation>Koko sana</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="139"/>
        <source>Regular Expression</source>
        <translation>Säännöllinen lauseke</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="171"/>
        <source>Search with regex...</source>
        <translation>Etsi käyttäen säännöllistä lauseketta...</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="120"/>
        <source>Next</source>
        <translation>Seuraava</translation>
    </message>
    <message>
        <location filename="../../searchbar.cpp" line="121"/>
        <source>Previous</source>
        <translation>Edellinen</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SessionDialog</name>
    <message>
        <location filename="../../sessionDialog.ui" line="14"/>
        <source>Session Manager</source>
        <translation>Istunnonhallinta</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="101"/>
        <location filename="../../sessionDialog.ui" line="270"/>
        <source>&amp;Remove</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="33"/>
        <source>&lt;b&gt;Save/Restore Session&lt;/b&gt;</source>
        <translation>&lt;b&gt;Tallenna/palauta istunto&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="56"/>
        <source>Filter...</source>
        <translation>Suodata...</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="69"/>
        <source>Open the selected session(s).</source>
        <translation>Avaa valittu tai valitut istunnot.</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="72"/>
        <location filename="../../sessionDialog.ui" line="275"/>
        <source>&amp;Open</source>
        <translation>&amp;Avaa</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="98"/>
        <location filename="../../sessionDialog.ui" line="108"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="115"/>
        <location filename="../../sessionDialog.ui" line="125"/>
        <source>Ctrl+Del</source>
        <translation>Ctrl+Del</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="159"/>
        <source>By default, all files that are opened in all
windows will be included in the saved session.</source>
        <translation>Oletuksena kaikki tiedostot kaikista auki olevista
ikkunoista sisällytetään tallennettuun istuntoon.</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="163"/>
        <source>Save only from this &amp;window</source>
        <translation>Tallenna vain tästä &amp;ikkunasta</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="180"/>
        <source>&lt;b&gt;Warning&lt;/b&gt;</source>
        <translation>&lt;b&gt;Varoitus&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="226"/>
        <location filename="../../session.cpp" line="328"/>
        <source>&amp;Yes</source>
        <translation>&amp;Kyllä</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="233"/>
        <source>&amp;No</source>
        <translation>&amp;Ei</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="283"/>
        <source>Re&amp;name</source>
        <translation>&amp;Nimeä uudelleen</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="118"/>
        <source>Remove &amp;All</source>
        <translation>Poista &amp;kaikki</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="260"/>
        <source>&amp;Close</source>
        <translation>&amp;Sulje</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="135"/>
        <source>Save the current session under the given title.</source>
        <translation>Tallenna aktiivinen istunto valitulla nimellä.</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="138"/>
        <source>&amp;Save</source>
        <translation>&amp;Tallenna</translation>
    </message>
    <message>
        <location filename="../../sessionDialog.ui" line="149"/>
        <source>Type a name to save session</source>
        <translation>Kirjoita nimi tallentaaksesi istunnon</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="179"/>
        <source>Nothing saved.&lt;br&gt;No file was opened.</source>
        <translation>Mitään ei tallennettu.&lt;br&gt;Yhtään tiedostoa ei ollut avattuna.</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="277"/>
        <source>No file exists or can be opened.</source>
        <translation>Tiedostoa ei ole olemassa tai sitä ei voida avata.</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="279"/>
        <source>Not all files exist or can be opened.</source>
        <translation>Joitakin tiedostoista ei ole olemassa, tai niitä ei muusta syystä voida avata.</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="314"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="333"/>
        <source>Do you really want to remove all saved sessions?</source>
        <translation>Haluatko varmasti poistaa kaikki tallennetut istunnot?</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="339"/>
        <source>Do you really want to remove the selected sessions?</source>
        <translation>Haluatko varmasti poistaa valitut istunnot?</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="341"/>
        <source>Do you really want to remove the selected session?</source>
        <translation>Haluatko varmasti poistaa valitun istunnon?</translation>
    </message>
    <message>
        <location filename="../../session.cpp" line="346"/>
        <source>A session with the same name exists.&lt;br&gt;Do you want to overwrite it?</source>
        <translation>Istunto samalla nimellä on jo olemassa.&lt;br&gt;Haluatko korvata sen?</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SidePane</name>
    <message>
        <location filename="../../sidepane.cpp" line="210"/>
        <source>Filter...</source>
        <translation>Suodata...</translation>
    </message>
</context>
<context>
    <name>FeatherPad::SpellDialog</name>
    <message>
        <location filename="../../spellDialog.ui" line="25"/>
        <source>Unknown word:</source>
        <translation>Tuntematon sana:</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="51"/>
        <source>Add To Dictionary</source>
        <translation>Lisää sanastoon</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="62"/>
        <source>Replace with:</source>
        <translation>Korvaa:</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="129"/>
        <source>Correct Once</source>
        <translation>Korjaa kerran</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="142"/>
        <source>Correct All</source>
        <translation>Korjaa kaikki</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="87"/>
        <source>Ignore Once</source>
        <translation>Jätä huomiotta kerran</translation>
    </message>
    <message>
        <location filename="../../spellDialog.ui" line="100"/>
        <source>Ignore All</source>
        <translation>Jätä huomiotta kaikki</translation>
    </message>
</context>
<context>
    <name>FeatherPad::TextEdit</name>
    <message>
        <location filename="../../textedit.cpp" line="148"/>
        <source>Double click to center current line</source>
        <translation>Kaksoisklikkaa keskittääksesi tämänhetkinen rivi</translation>
    </message>
</context>
</TS>
